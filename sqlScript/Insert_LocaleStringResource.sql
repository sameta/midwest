 If Not Exists (Select Id From LocaleStringResource where ResourceName = 'ContactUs.Enquiry' and ResourceValue='Query')
 Begin
    INSERT INTO [LocaleStringResource] ([ResourceValue],[ResourceName],[LanguageId])
    VALUES ('ContactUs.Enquiry','Query',1)
 End
If Not Exists (Select Id From LocaleStringResource where ResourceName = 'contactus.enquiry.hint' and ResourceValue='Enter your quiry.')
 Begin
    INSERT INTO [LocaleStringResource] ([ResourceValue],[ResourceName],[LanguageId])
    VALUES ('contactus.enquiry.hint','Enter your quiry.',1)
 End
If Not Exists (Select Id From LocaleStringResource where ResourceName = 'contactus.enquiry.required' and ResourceValue='Enter quiry')
 Begin
    INSERT INTO [LocaleStringResource] ([ResourceValue],[ResourceName],[LanguageId])
    VALUES ('contactus.enquiry.required','Enter quiry',1)
 End

If Not Exists (Select Id From LocaleStringResource where ResourceName = 'ContactUs.Enquiry.CompanyName' and ResourceValue='Company Name')
 Begin
    INSERT INTO [LocaleStringResource] ([ResourceValue],[ResourceName],[LanguageId])
    VALUES ('ContactUs.Enquiry.CompanyName','Company Name',1)
 End
If Not Exists (Select Id From LocaleStringResource where ResourceName = 'contactus.enquiry.CompanyName.hint' and ResourceValue='Enter your company name.')
 Begin
    INSERT INTO [LocaleStringResource] ([ResourceValue],[ResourceName],[LanguageId])
    VALUES ('contactus.enquiry.CompanyName.hint','Enter your company name.',1)
 End
