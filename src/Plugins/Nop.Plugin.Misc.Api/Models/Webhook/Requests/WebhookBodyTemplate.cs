﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Api.Models.Webhook.Requests
{
    public class WebhookBodyTemplate
    {
        public string accountCode { get; set; } = "${account-code}";

        public string resourceType { get; set; } = "${resource-type}";

        public string id { get; set; } = "${resource-id}";

        public string lifecycleEvent { get; set; } = "${lifecycle-event}";

        public string fullEvent { get; set; } = "${full-event}";

        public string raisedOn { get; set; } = "${raised-on}";

        public string brightpearlVersion { get; set; } = "${brightpearl-version}";

    }
}
