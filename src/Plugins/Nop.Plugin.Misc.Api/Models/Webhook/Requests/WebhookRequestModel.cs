﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Api.Models.Webhook.Requests
{
    public class WebhookRequestModel
    {
        public string subscribeTo { get; set; }
        public string httpMethod { get; set; }
        public string uriTemplate { get; set; }
        public string bodyTemplate { get; set; }
        public string contentType { get; set; }
        public bool idSetAccepted { get; set; }
        public int qualityOfService { get; set; }

    }
}
