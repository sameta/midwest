﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Nop.Plugin.Api.Areas.Admin.Models.Product
{
    public class ProductRequestModel
    {
        // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse);
        public class BundleComponent
        {
            public int productId { get; set; }
            public int productQuantity { get; set; }
        }

        public class Category
        {
            public string categoryCode { get; set; }
        }

        public class Composition
        {
            public bool bundle { get; set; }
            public List<BundleComponent> bundleComponents { get; set; } = new List<BundleComponent>();
        }

        public class Description
        {
            public string languageCode { get; set; }
            public string text { get; set; }
            public string format { get; set; }
        }

        public class Dimensions
        {
            public string width { get; set; } = "0";
            public string length { get; set; } = "0";
            public string height { get; set; } = "0";
        }

        public class FinancialDetails
        {
            public bool taxable { get; set; }
            public TaxCode taxCode { get; set; } = new TaxCode();   
        }

        public class Identity
        {
            public string sku { get; set; }
            public string ean { get; set; }
            public string upc { get; set; }
            public string isbn { get; set; }
            public string barcode { get; set; }
        }

        public class Reporting
        {
            public int seasonId { get; set; }
            public int categoryId { get; set; }
            public int subcategoryId { get; set; }
        }

        public class ProductRequestRoot
        {
            public int brandId { get; set; }

            [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
            public int? collectionId { get; set; }

            [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
            public int? productTypeId { get; set; }
            public Identity identity { get; set; } = new Identity();
            public Stock stock { get; set; } = new Stock();
            public FinancialDetails financialDetails { get; set; } = new FinancialDetails();
            public List<SalesChannel> salesChannels { get; set; } = new List<SalesChannel>();
            public List<Variation> variations { get; set; } = new List<Variation>();
            public List<int> seasonIds { get; set; } = new List<int> ();
            public Composition composition { get; set; } = new Composition ();
            public string nominalCodeStock { get; set; }
            public string nominalCodePurchases { get; set; }
            public string nominalCodeSales { get; set; }

            [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
            public Reporting reporting { get; set; } = null;
        }

        public class SalesChannel
        {
            public string salesChannelName { get; set; }
            public string productName { get; set; }
            public string productCondition { get; set; }
            public List<Category> categories { get; set; } = new List<Category>();
            public Description description { get; set; } = new Description();
            public ShortDescription shortDescription { get; set; } = new ShortDescription();
        }

        public class ShortDescription
        {
            public string languageCode { get; set; }
            public string text { get; set; }
            public string format { get; set; }
        }

        public class Stock
        {
            public bool stockTracked { get; set; }
            public Weight weight { get; set; } = new Weight();
            public Dimensions dimensions { get; set; } = new Dimensions();
        }

        public class TaxCode
        {
            public int id { get; set; }
            public string code { get; set; }
        }

        public class Variation
        {
            public int optionId { get; set; }
            public int optionValueId { get; set; }
        }

        public class Weight
        {
            public double magnitude { get; set; }
        }


    }
}
