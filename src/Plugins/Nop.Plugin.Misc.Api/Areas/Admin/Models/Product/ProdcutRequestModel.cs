﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Api.Areas.Admin.Models.Product
{
	public class ProdcutRequestModel
	{
		public class BundleComponent
		{
			public int productId { get; set; }
			public int productQuantity { get; set; }
		}

		public class Category
		{
			public string categoryCode { get; set; }
		}

		public class Composition
		{
			public bool bundle { get; set; }
			public List<BundleComponent> bundleComponents { get; set; }
		}

		public class Description
		{
			public string languageCode { get; set; }
			public string text { get; set; }
			public string format { get; set; }
		}

		public class FinancialDetails
		{
			public bool taxable { get; set; }
			public TaxCode taxCode { get; set; }
		}

		public class Identity
		{
			public string sku { get; set; }
			public string ean { get; set; }
			public string upc { get; set; }
			public string isbn { get; set; }
			public string barcode { get; set; }
		}

		public class ProductResponses
		{
			public int brandId { get; set; }
			public int collectionId { get; set; }
			public int productTypeId { get; set; }
			public Identity identity { get; set; }
			public Stock stock { get; set; }
			public FinancialDetails financialDetails { get; set; }
			public List<SalesChannel> salesChannels { get; set; }
			public List<int> seasonIds { get; set; }
			public Composition composition { get; set; }
		}

		public class SalesChannel
		{
			public string salesChannelName { get; set; }
			public string productName { get; set; }
			public string productCondition { get; set; }
			public List<Category> categories { get; set; }
			public Description description { get; set; }
			public ShortDescription shortDescription { get; set; }
		}

		public class ShortDescription
		{
			public string languageCode { get; set; }
			public string text { get; set; }
			public string format { get; set; }
		}

		public class Stock
		{
			public bool stockTracked { get; set; }
			public Weight weight { get; set; }
		}

		public class TaxCode
		{
			public int id { get; set; }
			public string code { get; set; }
		}

		public class Weight
		{
			public decimal magnitude { get; set; }
		}

	}
}
