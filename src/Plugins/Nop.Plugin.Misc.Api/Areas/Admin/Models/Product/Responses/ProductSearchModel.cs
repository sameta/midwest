﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Api.Areas.Admin.Models.Product
{
    public class ProductSearchModel
    {
        public int productId { get; set; }
        public string productName { get; set; }

        public string SKU { get; set; }

        public string barcode { get; set; }

        public string EAN { get; set; }

        public string UPC { get; set; }

        public string ISBN { get; set; }

        public string MPN { get; set; }

        public int stockTracked { get; set; }

        public string salesChannelName { get; set; }

        public string createdOn { get; set; }

        public string updatedOn { get; set; }

        public int brightpearlCategoryCode { get; set; }

        public int productGroupId { get; set; }

        public int brandId { get; set; }

        public int productTypeId { get; set; }

        public string productStatus { get; set; }

        public int primarySupplierId { get; set; }

    }
}
