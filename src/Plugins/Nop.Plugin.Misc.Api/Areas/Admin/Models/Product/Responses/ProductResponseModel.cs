﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Api.Areas.Admin.Models.Product
{
    public class ProductResponseModel
    {
        // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse);
        public class Category
        {
            public string categoryCode { get; set; }
        }

        public class Composition
        {
            public bool bundle { get; set; }
        }

        public class Description
        {
            public string languageCode { get; set; }
            public string text { get; set; }
            public string format { get; set; }
        }

        public class Dimensions
        {
            public int length { get; set; }
            public int height { get; set; }
            public int width { get; set; }
            public int volume { get; set; }
        }

        public class FinancialDetails
        {
            public bool taxable { get; set; }
            public TaxCode taxCode { get; set; }
        }

        public class Identity
        {
            public string sku { get; set; }
            public string upc { get; set; }
            public string mpn { get; set; }
            public string barcode { get; set; }
        }

        public class Reporting
        {
        }

        public class Response
        {
            public int id { get; set; }
            public int brandId { get; set; }
            public int productTypeId { get; set; }
            public Identity identity { get; set; }
            public int productGroupId { get; set; }
            public bool featured { get; set; }
            public Stock stock { get; set; }
            public FinancialDetails financialDetails { get; set; }
            public List<SalesChannel> salesChannels { get; set; }
            public Composition composition { get; set; }
            public List<Variation> variations { get; set; }
            public DateTime createdOn { get; set; }
            public DateTime updatedOn { get; set; }
            public Warehouses warehouses { get; set; }
            public string nominalCodeStock { get; set; }
            public string nominalCodePurchases { get; set; }
            public string nominalCodeSales { get; set; }
            public List<object> seasonIds { get; set; }
            public Reporting reporting { get; set; }
            public int primarySupplierId { get; set; }
            public string status { get; set; }
            public string salesPopupMessage { get; set; }
            public string warehousePopupMessage { get; set; }
            public long version { get; set; }
        }

        public class ProductRoot
        {
            public List<Response> response { get; set; }
        }

        public class SalesChannel
        {
            public string salesChannelName { get; set; }
            public string productName { get; set; }
            public string productCondition { get; set; }
            public List<Category> categories { get; set; }
            public Description description { get; set; }
            public ShortDescription shortDescription { get; set; }
        }

        public class ShortDescription
        {
            public string languageCode { get; set; }
            public string text { get; set; }
            public string format { get; set; }
        }

        public class Stock
        {
            public bool stockTracked { get; set; }
            public Weight weight { get; set; }
            public Dimensions dimensions { get; set; }
        }

        public class TaxCode
        {
            public int id { get; set; }
            public string code { get; set; }
        }

        public class Variation
        {
            public int optionId { get; set; }
            public string optionName { get; set; }
            public int optionValueId { get; set; }
            public string optionValue { get; set; }
        }

        public class Warehouses
        {
        }

        public class Weight
        {
            public decimal magnitude { get; set; }
        }

    }

}
