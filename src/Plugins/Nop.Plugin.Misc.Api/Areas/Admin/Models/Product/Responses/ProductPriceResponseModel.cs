﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Api.Areas.Admin.Models.Product
{
    public class ProductPriceResponseModel
    {
        // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse);
        public class PriceList
        {
            public int priceListId { get; set; }
            public string currencyCode { get; set; }
            public int currencyId { get; set; }
            public QuantityPrice quantityPrice { get; set; }
        }

        public class QuantityPrice
        {
        }

        public class Response
        {
            public int productId { get; set; }
            public List<PriceList> priceLists { get; set; }
        }

        public class ProductPriceRoot
        {
            public List<Response> response { get; set; }
        }


    }
}
