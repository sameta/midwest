﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Api.Areas.Admin.Models.TirePrice
{
    public class TirePriceModel
    {
        public class PriceList
        {
            public int priceListId { get; set; }
            public QuantityPrice quantityPrice { get; set; }
            public string sku { get; set; }
        }

        public class QuantityPrice
        {
            public string Quantity { get; set; }
            public string Price { get; set; }
        }

        public class TirePrice
        {
            public List<PriceList> priceLists { get; set; }
        }
    }
}
