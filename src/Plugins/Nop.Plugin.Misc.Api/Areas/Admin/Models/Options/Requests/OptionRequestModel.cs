﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Api.Areas.Admin.Models.Options.Requests
{
    public class OptionRequestModel
    {
        public string name { get; set; }

        public int sortOrder { get; set; } = 0;
    }
}
