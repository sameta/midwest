﻿using Nop.Web.Framework.Mvc.ModelBinding;

namespace Nop.Plugin.Api.Areas.Admin.Models
{
    public class ConfigurationModel
    {
        [NopResourceDisplayName("Plugins.Api.Admin.EnableApi")]
        public bool EnableApi { get; set; }

        public bool EnableApi_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.Api.Admin.TokenExpiryInDays")]
        public int TokenExpiryInDays { get; set; }

        public bool TokenExpiryInDays_OverrideForStore { get; set; }

        public int ActiveStoreScopeConfiguration { get; set; }

        [NopResourceDisplayName("Plugin.Api.BrightPearl.Field.Username")]
        public string UserName { get; set; }

        [NopResourceDisplayName("Plugin.Api.BrightPearl.Field.Password")]
        public string Password { get; set; }

        [NopResourceDisplayName("Plugin.Api.BrightPearl.Field.ClientId")]
        public string ClientId { get; set; }

        [NopResourceDisplayName("Plugin.Api.BrightPearl.Field.ClientSecret")]
        public string ClientSecret { get; set; }

        [NopResourceDisplayName("Plugin.Api.BrightPearl.Field.RedirectUri")]
        public string RedirectUri { get; set; }

        [NopResourceDisplayName("Plugin.Api.BrightPearl.Field.Token")]
        public string Token { get; set; }

        [NopResourceDisplayName("Plugin.Api.BrightPearl.Field.ApiUrl")]
        public string ApiUrl { get; set; }

        [NopResourceDisplayName("Plugin.Api.BrightPearl.Field.BrightPearlAppRef")]
        public string BrightPearlAppRef { get; set; }

        [NopResourceDisplayName("Plugin.Api.BrightPearl.Field.BrightPearlStaffToken")]
        public string BrightPearlStaffToken { get; set; }

        [NopResourceDisplayName("Plugin.Api.BrightPearl.Field.PublicApiUrl")]
        public string PublicApiUrl { get; set; }

        [NopResourceDisplayName("Plugin.Api.BrightPearl.Field.GrantType")]
        public string GrantType { get; set; }

        public string ApiDomain { get; set; }

        public string RefreshToken { get; set; }

        public string ExpirationTime { get; set; }
        public string InstallationInstanceId { get; set; }
    }
}
