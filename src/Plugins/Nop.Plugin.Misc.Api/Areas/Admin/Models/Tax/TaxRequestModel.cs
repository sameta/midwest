﻿using System.Collections.Generic;

namespace Nop.Plugin.Api.Areas.Admin.Models.Tax
{
	public class TaxRequestModel
	{
		public class TaxResponse
		{
			public int id { get; set; }
			public string code { get; set; }
			public string description { get; set; }
			public decimal? rate { get; set; }
		}

		public class Tax
		{
			public IEnumerable<TaxResponse> response { get; set; }
		}
	}
}
