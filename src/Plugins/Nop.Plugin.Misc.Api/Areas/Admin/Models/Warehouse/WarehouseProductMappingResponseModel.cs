﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Api.Areas.Admin.Models.Warehouse
{
    public class WarehouseProductMappingResponseModel
    {
        public int productId { get; set; }
        public int warehouseMWId { get;set; }
    }
}
