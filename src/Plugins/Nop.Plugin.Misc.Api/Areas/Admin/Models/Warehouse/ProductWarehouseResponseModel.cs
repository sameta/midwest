﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Api.Areas.Admin.Models.Warehouse
{
    public class ProductWarehouseResponseModel
    {
        public string Id { get; set; }

        public string defaultLocationId { get; set; }

        public string overflowLocationId { get; set; }

        public string reorderLevel { get; set; }
        public string reorderQuantity { get; set; }

    }
}

