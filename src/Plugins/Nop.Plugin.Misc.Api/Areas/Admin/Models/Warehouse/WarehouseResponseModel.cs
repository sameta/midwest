﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Api.Areas.Admin.Models.Warehouse
{
    public record WarehouseResponseModel
    {
        // Root myDeserializedClass = JsonConvert.DeserializeObject<WarehouseRoot>(myJsonResponse);
        public class Address
        {
            public int addressId { get; set; }
            public int customerId { get; set; }
            public string streetAddress { get; set; }
            public string postcode { get; set; }
            public string city { get; set; }
            public string state { get; set; }
            public int countryId { get; set; }
            public string countryIsoCode2 { get; set; }
        }

        public class CollectionSla
        {
            public int value { get; set; }
            public string timeUnits { get; set; }
        }

        public class ExternalTransferSla
        {
            public int warehouseId { get; set; }
            public int value { get; set; }
            public string timeUnits { get; set; }
        }

        public class Period
        {
            public int dayOfWeek { get; set; }
            public string openingHours { get; set; }
            public string closingHours { get; set; }
        }

        public class Response
        {
            public string name { get; set; }
            public int id { get; set; }
            public string typeCode { get; set; }
            public string typeDescription { get; set; }
            public Address address { get; set; }
            public bool clickAndCollectEnabled { get; set; }
            public WeeklyOperatingPeriods weeklyOperatingPeriods { get; set; }
            public CollectionSla collectionSla { get; set; }
            public List<ExternalTransferSla> externalTransferSlas { get; set; }
        }

        public class WarehouseRoot
        {
            public List<Response> response { get; set; }
        }

        public class WeeklyOperatingPeriods
        {
            public string timeZone { get; set; }
            public List<Period> periods { get; set; }
        }


    }
}
