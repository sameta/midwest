﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Api.Areas.Admin.Models.Pricing.Responses
{
    public class PriceListResponseModel
    {
        // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse);
        public class Name
        {
            public string languageCode { get; set; }
            public string text { get; set; }
            public string format { get; set; }
        }

        public class Response
        {
            public int id { get; set; }
            public Name name { get; set; }
            public string code { get; set; }
            public string currencyCode { get; set; }
            public string currencySymbol { get; set; }
            public int currencyId { get; set; }
            public string priceListTypeCode { get; set; }
            public bool gross { get; set; }
        }

        public class PriceList
        {
            public List<Response> response { get; set; }
        }


    }
}
