﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Api.Areas.Admin.Models.Manufacturer
{
    public class ManufacturerResponseModel
    {
        public class ManufacturerResponse
        {
            public int id { get; set; }
            public string name { get; set; }
            public string description { get; set; }
        }

        public class ManufacturerRoot
        {
            public List<ManufacturerResponse> response { get; set; }
        }

    }

}
