﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Api.Areas.Admin.Models.Manufacturer
{
	public class ManufacturerRequestModel
	{
        public string name { get; set; }
        public string description { get; set; }
    }
}
