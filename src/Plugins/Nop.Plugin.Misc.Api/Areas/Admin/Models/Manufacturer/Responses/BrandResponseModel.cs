﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Api.Areas.Admin.Models.Manufacturer.Responses
{
    public class BrandResponseModel
    {
        // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse);
        public class Column
        {
            public string name { get; set; }
            public bool sortable { get; set; }
            public bool filterable { get; set; }
            public string reportDataType { get; set; }
            public List<object> referenceData { get; set; }
            public bool isFreeTextSearchable { get; set; }
            public bool required { get; set; }
        }

        public class Filterable
        {
            public string name { get; set; }
            public bool sortable { get; set; }
            public bool filterable { get; set; }
            public string reportDataType { get; set; }
            public List<object> referenceData { get; set; }
            public bool isFreeTextSearchable { get; set; }
            public bool required { get; set; }
        }

        public class MetaData
        {
            public bool morePagesAvailable { get; set; }
            public int resultsAvailable { get; set; }
            public int resultsReturned { get; set; }
            public int firstResult { get; set; }
            public int lastResult { get; set; }
            public List<Column> columns { get; set; }
            public List<Sorting> sorting { get; set; }
        }

        public class Reference
        {
        }

        public class Response
        {
            public List<List<object>> results { get; set; }
            public MetaData metaData { get; set; }
        }

        public class BrandResponseRoot
        {
            public Response response { get; set; }
            public Reference reference { get; set; }
        }

        public class Sorting
        {
            public Filterable filterable { get; set; }
            public string direction { get; set; }
        }


    }
}
