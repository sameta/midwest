﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Api.Areas.Admin.Models.CustomFields.Responses
{
    public class CustomFieldMetadataResponseModel
    {
        // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse);

        public class Options
        {
        }

        public class Response
        {
            public int id { get; set; }
            public string code { get; set; }
            public string name { get; set; }
            public string customFieldType { get; set; }
            public bool required { get; set; }
            public Options options { get; set; }
        }

        public class Root
        {
            public List<Response> response { get; set; }
        }


    }
}
