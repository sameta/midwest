﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Api.Areas.Admin.Models.CustomFields.Responses
{
    public class ProductCustomFieldResponse
    {
        public string PCF_FABRIC { get; set; }
        public string PCF_LINING { get; set; }
        public string PCF_PPE { get; set; }
        public bool PCF_HIVIS { get; set; }
        public string PCF_FIT { get; set; }
        public string PCF_SS { get; set; }
        public CustomFieldMetadataOptionsResponseModel PCF_GENDER { get; set; }
        public string PCF_IMAGEF { get; set; }
        public bool PCF_FR { get; set; }
        public string PCF_IMAGEB { get; set; }
    }

    public class Root
    {
        public List<ProductCustomFieldResponse> response { get; set; }
    }
}
