﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Api.Areas.Admin.Models.CustomFields.Responses
{
    public  class CustomFieldMetadataOptionsResponseModel
    {
        public int id { get; set; }
        public string value { get; set; }
    }
}
