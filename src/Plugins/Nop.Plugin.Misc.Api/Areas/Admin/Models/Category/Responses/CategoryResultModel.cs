﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Api.Areas.Admin.Models.Category.Responses
{
    public class CategoryResultModel
    {
        public int id { get; set; }
        public string name { get; set; }
        public int parentId { get; set; }
    }

}
