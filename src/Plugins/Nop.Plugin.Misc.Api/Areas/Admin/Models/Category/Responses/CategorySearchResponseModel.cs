﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Api.Areas.Admin.Models.Category.Responses
{
    public class CategorySearchResponseModel
    {
        // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse);
        public class Description
        {
            public string languageCode { get; set; }
            public string text { get; set; }
            public string format { get; set; }
        }

        public class Response
        {
            public int id { get; set; }
            public string name { get; set; }
            public int parentId { get; set; }
            public bool active { get; set; }
            public DateTime createdOn { get; set; }
            public int createdById { get; set; }
            public DateTime updatedOn { get; set; }
            public int updatedById { get; set; }
            public Description description { get; set; }
        }

        public class CategoryRoot
        {
            public List<Response> response { get; set; }
        }


    }


}
