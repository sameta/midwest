﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Api.Areas.Admin.Models.Customer
{
    public class TaxCodeResponse
    {
        public class Response
        {
            public int id { get; set; }
            public string code { get; set; }
            public string description { get; set; }
            public int? rate { get; set; }
        }

        public class Root
        {
            public List<Response> response { get; set; }
        }
    }
}
