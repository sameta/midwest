﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Api.Areas.Admin.Models.Customer
{
    public class NominalCodeResponseModel
    {
        public class Response
        {
            public int id { get; set; }
            public string code { get; set; }
            public string name { get; set; }
            public Type type { get; set; }
            public bool bank { get; set; }
            public bool expense { get; set; }
            public bool discount { get; set; }
            public bool editable { get; set; }
            public bool active { get; set; }
            public int taxCode { get; set; }
            public string chartMapCode { get; set; }
            public bool reconcile { get; set; }
        }

        public class Root
        {
            public List<Response> response { get; set; }
        }

        public class Type
        {
            public int id { get; set; }
        }
    }
}
