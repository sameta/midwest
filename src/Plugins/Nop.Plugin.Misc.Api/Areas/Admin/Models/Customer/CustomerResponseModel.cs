﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Api.Areas.Admin.Models.Customer
{
    public record CustomerResponseModel
    {
        // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse);
        public class Address
        {
            public string addressLine1 { get; set; }
            public string addressLine2 { get; set; }
            public string addressLine3 { get; set; }
            public string addressLine4 { get; set; }
            public string postalCode { get; set; }
            public string countryIsoCode { get; set; }
            public int countryId { get; set; }
        }


        public class Aliases
        {
            public string eBay { get; set; }
        }

        public class Assignment
        {
            public Current current { get; set; }
        }

        public class Communication
        {
            public Emails emails { get; set; }
            public Telephones telephones { get; set; }
            public MessagingVoips messagingVoips { get; set; }
            public Websites websites { get; set; }
        }

        public class ContactStatus
        {
            public Current current { get; set; }
            public List<History> history { get; set; }
        }

        public class Current
        {
            public string contactStatusName { get; set; }
            public int contactStatusId { get; set; }
            public int staffOwnerContactId { get; set; }
            public int departmentId { get; set; }
            public int leadSourceId { get; set; }
            public string accountReference { get; set; }
            public int contactGroupId { get; set; }
        }

        public class CustomFields
        {
            public PCFSELECT PCF_SELECT { get; set; }
            public DateTime PCF_DATE { get; set; }
            public bool PCF_YESNO { get; set; }
            public string PCF_FREETXT { get; set; }
            public string PCF_LARGETXT { get; set; }
            public int PCF_NUMERIC { get; set; }
        }

        public class Emails
        {
            public PRI PRI { get; set; }
            public SEC SEC { get; set; }
            public TER TER { get; set; }
        }

        public class FinancialDetails
        {
            public int priceListId { get; set; }
            public string nominalCode { get; set; }
            public int taxCodeId { get; set; }
            public string creditLimit { get; set; }
            public int creditTermDays { get; set; }
            public int currencyId { get; set; }
            public double discountPercentage { get; set; }
            public string taxNumber { get; set; }
        }

        public class History
        {
            public string contactStatusName { get; set; }
            public int contactStatusId { get; set; }
            public DateTime occuredOn { get; set; }
            public int staffContactId { get; set; }
        }

        public class MarketingDetails
        {
            public bool isReceiveEmailNewsletter { get; set; }
        }

        public class MessagingVoips
        {
            public string SKP { get; set; }
        }

        public class Organisation
        {
            public int organisationId { get; set; }
            public string name { get; set; }
            public string jobTitle { get; set; }
        }

        public class Pagination
        {
            public bool morePagesAvailable { get; set; }
            public int resultsReturned { get; set; }
            public int firstResult { get; set; }
            public int lastResult { get; set; }
        }

        public class PCFSELECT
        {
            public int id { get; set; }
            public string value { get; set; }
        }

        public class PostAddressIds
        {
            public int DEF { get; set; }
            public int BIL { get; set; }
            public int DEL { get; set; }
        }

        public class PostalAddresses
        {
            
            public Address address { get; set; }

        }

        public class PRI
        {
            public string email { get; set; }
        }

        public class RelationshipToAccount
        {
            public bool isSupplier { get; set; }
            public bool isStaff { get; set; }
        }

        public class Response
        {
            public int contactId { get; set; }
            public bool isPrimaryContact { get; set; }
            public string salutation { get; set; }
            public string firstName { get; set; }
            public string lastName { get; set; }
            public PostAddressIds postAddressIds { get; set; }
            public PostalAddresses postalAddresses { get; set; }
            public Communication communication { get; set; }
            public ContactStatus contactStatus { get; set; }
            public RelationshipToAccount relationshipToAccount { get; set; }
            public MarketingDetails marketingDetails { get; set; }
            public FinancialDetails financialDetails { get; set; }
            public Assignment assignment { get; set; }
            public Organisation organisation { get; set; }
            public string tradeStatus { get; set; }
            public int createdByid { get; set; }
            public DateTime createdOn { get; set; }
            public DateTime updatedOn { get; set; }
            public DateTime lastContactedOn { get; set; }
            public CustomFields customFields { get; set; }
            public string contactTags { get; set; }
            public Aliases aliases { get; set; }
            public int companyId { get; set; }
        }

        public class Root
        {
            public List<Response> response { get; set; }
            public Pagination pagination { get; set; }
            public List<Sorting> sorting { get; set; }
        }

        public class SEC
        {
            public string email { get; set; }
        }

        public class Sorting
        {
            public string field { get; set; }
            public string direction { get; set; }
        }

        public class Telephones
        {
            public string PRI { get; set; }
            public string SEC { get; set; }
            public string MOB { get; set; }
        }

        public class TER
        {
            public string email { get; set; }
        }

        public class Websites
        {
            public string PRI { get; set; }
        }



    }
}
