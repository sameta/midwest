﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Api.Areas.Admin.Models.Customer
{
    public class PostaladdressModel
    {
        [JsonProperty("addressId")]
        public long AddressId { get; set; }

        [JsonProperty("contactId")]
        public long ContactId { get; set; }

        [JsonProperty("addressLine1")]
        public string AddressLine1 { get; set; }

        [JsonProperty("addressLine2")]
        public string AddressLine2 { get; set; }

        [JsonProperty("addressLine3")]
        public string AddressLine3 { get; set; }

        [JsonProperty("addressLine4")]
        public string AddressLine4 { get; set; }

        [JsonProperty("postalCode")]
        public string PostalCode { get; set; }

        [JsonProperty("countryIsoCode")]
        public string CountryIsoCode { get; set; }

        [JsonProperty("countryIsoCode2")]
        public string CountryIsoCode2 { get; set; }

        [JsonProperty("countryId")]
        public int CountryId { get; set; }
    }
}
