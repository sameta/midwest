﻿using Newtonsoft.Json;
using Nop.Core;
using Nop.Plugin.Api.Areas.Admin.Models.Category;
using Nop.Plugin.Api.Areas.Admin.Models.Manufacturer;
using Nop.Plugin.Api.Domain;
using Nop.Plugin.Api.Services;
using Nop.Services.Catalog;
using Nop.Services.Configuration;
using Nop.Web.Framework.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Api.Areas.Admin.Controllers
{
	public class ManufacturerController : BasePluginController
	{
		private readonly IManufacturerService _manufacturerServices;
		private readonly IManufacturerServiceMW _manufacturerServicesMW;
		private readonly IApiHttpRequestService _apiHttpRequestService;
		private readonly IStoreContext _storeContext;
		private readonly ISettingService _settingService;
		protected const string ContentType = "application/json";
		private readonly HttpClient _httpClient;
        public ManufacturerController(IManufacturerService manufacturerService, IManufacturerServiceMW manufacturerServiceMW,
			IApiHttpRequestService apiHttpRequestService, IStoreContext storeContext, ISettingService settingService,
			HttpClient httpClient)
        {
			_manufacturerServices = manufacturerService;
			_manufacturerServicesMW = manufacturerServiceMW;
			_apiHttpRequestService = apiHttpRequestService;
			_storeContext = storeContext;
			_settingService = settingService;
			_httpClient = httpClient;
        }
		public async Task InsertNopManufacturerToBrightpearl()
		{
			var listOfManufacturer = await _manufacturerServicesMW.GetManufacturerAsync();
			foreach (var manufacturer in listOfManufacturer)
			{
				ManufacturerRequestModel manufacturerRequestModel = new ManufacturerRequestModel();
				manufacturerRequestModel.name = manufacturer.Name;
				manufacturerRequestModel.description = manufacturer.Description;
				var storeScope = await _storeContext.GetActiveStoreScopeConfigurationAsync();
				var settings = await _settingService.LoadSettingAsync<ApiSettings>(storeScope);
				_httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
				_httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(settings.Token);
				var body = JsonConvert.SerializeObject(manufacturerRequestModel, new JsonSerializerSettings
				{
					NullValueHandling = NullValueHandling.Ignore
				});
				var content = new StringContent(body, Encoding.UTF8, ContentType);
				var response = await _httpClient.PostAsync(settings.PublicApiUrl, content).ConfigureAwait(false);
				response.EnsureSuccessStatusCode();
				var httpresponsestring = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
				var responseId = JsonConvert.DeserializeObject<int>(httpresponsestring);
				manufacturer.MWMId = responseId;
				await _manufacturerServices.UpdateManufacturerAsync(manufacturer);
			}
		}
	}
}
