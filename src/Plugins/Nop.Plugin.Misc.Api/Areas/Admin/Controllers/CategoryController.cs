﻿using Microsoft.AspNetCore.Mvc;
using Nop.Web.Framework.Mvc.Filters;
using Nop.Web.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Web.Framework.Controllers;
using Nop.Services.Catalog;
using Nop.Plugin.Api.Services;
using Nop.Core;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using Nop.Plugin.Api.Domain;
using Nop.Services.Configuration;
using Microsoft.AspNetCore.Http;
using Nop.Plugin.Api.Areas.Admin.Models.Category.Requests;

namespace Nop.Plugin.Api.Areas.Admin.Controllers
{
    [AuthorizeAdmin]
	[Area(AreaNames.Admin)]
	
	public class CategoryController : BasePluginController
	{
		private readonly ICategoryServiceMW _categoryServices;
		private readonly ICategoryService _category;
		private readonly IApiHttpRequestService _apiHttpRequestService;
		private readonly IStoreContext _storeContext;
		private readonly ISettingService _settingService;
		protected const string ContentType = "application/json";
		private readonly HttpClient _httpClient;
		public CategoryController(ICategoryServiceMW categoryService, IApiHttpRequestService apiHttpRequestService, IStoreContext storeContext, HttpClient httpClient, ISettingService settingService, ICategoryService category)
        {
			_categoryServices = categoryService;
			_apiHttpRequestService = apiHttpRequestService;
			_storeContext = storeContext;
			_httpClient = httpClient;
			_settingService = settingService;
			_category = category;
        }
        public async Task InsertNopCategoryToBrightpearl()
		{
			var listOfCategories =await _categoryServices.GetCategoriesAsync();
			foreach (var category in listOfCategories) 
			{
				CategoryRequestModel categoryRequestModel = new CategoryRequestModel();
				categoryRequestModel.name=category.Name;
				categoryRequestModel.parentId = category.Id;
				var storeScope = await _storeContext.GetActiveStoreScopeConfigurationAsync();
				var settings = await _settingService.LoadSettingAsync<ApiSettings>(storeScope);
				_httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
				_httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(settings.Token);
				var body = JsonConvert.SerializeObject(categoryRequestModel,new JsonSerializerSettings
				{
					NullValueHandling = NullValueHandling.Ignore
				});
				var content = new StringContent(body, Encoding.UTF8, ContentType);
				var response = await _httpClient.PostAsync(settings.PublicApiUrl,content).ConfigureAwait(false);
				response.EnsureSuccessStatusCode();
				var httpresponsestring = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
				var responseId = JsonConvert.DeserializeObject<int>(httpresponsestring);
				category.MWCId= responseId;
				await _category.UpdateCategoryAsync(category);
			}
		}
	}
}
