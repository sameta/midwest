﻿using DocumentFormat.OpenXml.EMMA;
using Newtonsoft.Json.Linq;
using Nop.Core.Domain.Tax;
using Nop.Plugin.Api.Areas.Admin.Models.Category;
using Nop.Plugin.Api.Areas.Admin.Models.Tax;
using Nop.Plugin.Api.Services;
using Nop.Services.Tax;
using Nop.Web.Framework.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Nop.Plugin.Api.Areas.Admin.Models.Tax.TaxRequestModel;

namespace Nop.Plugin.Api.Areas.Admin.Controllers
{
	public class GetTaxContoller : BasePluginController
	{
		private readonly ITaxCategoryService _taxCategoryService;
		private readonly IApiHttpRequestService _apiHttpRequestService;
		private readonly ITaxCategoryServiceMW _taxCategoryServiceMW;
		private readonly ITaxRateServices _taxRateServices;
		public GetTaxContoller(ITaxCategoryService taxCategoryService, IApiHttpRequestService apiHttpRequestService, ITaxCategoryServiceMW taxCategoryServiceMW,
			ITaxRateServices taxRateServices)
        {
             _taxCategoryService= taxCategoryService ;
			 _apiHttpRequestService= apiHttpRequestService ;
			_taxCategoryServiceMW = taxCategoryServiceMW;
			_taxRateServices = taxRateServices ;
        }

        public async Task GetTaxBrightPerl()
        {
			var response = await _apiHttpRequestService.HttpGetRequest(null, "accounting-service/tax-code");
			var model = JToken.Parse(response.Content.ReadAsStringAsync().Result).ToObject<IEnumerable<TaxRequestModel.TaxResponse>>();
			foreach (var item in model)
			{
				TaxCategory taxCategory = new TaxCategory();
				TaxRates taxRates = new TaxRates();
				taxCategory.MWId= item.id;
				taxCategory.Name = item.code;
				taxCategory.Description = item.description;
				await _taxCategoryService.InsertTaxCategoryAsync(taxCategory);
				var Id =_taxCategoryServiceMW.GetTaxCategoryByIdAsync(item.id);
				taxRates.TaxCategoryId = Id.Id;
				taxRates.Percentage=item.rate;
				await _taxRateServices.InsertTaxRatesAsync(taxRates);
			}
		}
    }
}
