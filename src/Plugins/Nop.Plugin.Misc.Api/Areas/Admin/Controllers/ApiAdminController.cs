﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json.Nodes;
using System.Threading.Tasks;
using DocumentFormat.OpenXml.Bibliography;
using DocumentFormat.OpenXml.Drawing.Diagrams;
using DocumentFormat.OpenXml.Wordprocessing;
using Microsoft.AspNetCore.Components.Server.ProtectedBrowserStorage;
using Microsoft.AspNetCore.Mvc;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Nop.Core;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Directory;
using Nop.Core.Domain.Shipping;
using Nop.Plugin.Api.Areas.Admin.Models;
using Nop.Plugin.Api.Areas.Admin.Models.Category;
using Nop.Plugin.Api.Areas.Admin.Models.Category.Responses;
using Nop.Plugin.Api.Areas.Admin.Models.Customer;
using Nop.Plugin.Api.Areas.Admin.Models.Manufacturer;
using Nop.Plugin.Api.Areas.Admin.Models.Product;
using Nop.Plugin.Api.Areas.Admin.Models.Warehouse;
using Nop.Plugin.Api.Domain;
using Nop.Plugin.Api.Helpers;
using Nop.Plugin.Api.MappingExtensions;
using Nop.Plugin.Api.Services;
using Nop.Services.Catalog;
using Nop.Services.Common;
using Nop.Services.Configuration;
using Nop.Services.Customers;
using Nop.Services.Directory;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Messages;
using Nop.Services.Seo;
using Nop.Services.Shipping;
using Nop.Web.Framework;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Mvc.Filters;
using NUglify.JavaScript;
using Org.BouncyCastle.Asn1.Cmp;
using static Nop.Plugin.Api.Areas.Admin.Models.Warehouse.WarehouseResponseModel;
using static Nop.Plugin.Api.Areas.Admin.Models.Product.ProductRequestModel;
using Category = Nop.Core.Domain.Catalog.Category;

namespace Nop.Plugin.Api.Areas.Admin.Controllers
{
    [AuthorizeAdmin]
	[Area(AreaNames.Admin)]
	[AutoValidateAntiforgeryToken]
	public class ApiAdminController : BasePluginController
	{
		private readonly ICustomerActivityService _customerActivityService;
		private readonly ILocalizationService _localizationService;
		private readonly INotificationService _notificationService;
		private readonly ISettingService _settingService;
		private readonly IStoreContext _storeContext;
		private readonly IApiHttpRequestService _apiHttpRequestService;

		private readonly IUrlRecordService _urlRecordService;

		private readonly IProductService _productService;

		private readonly CatalogSettings _catalogSettings;

		private readonly ICategoryService _categoryServices;
		private readonly IManufacturerService _manufacturerServices;
		private readonly IManufacturerServiceMW _manufacturerServicesMW;
		private readonly ICategoryServiceMW _categoryServicesMW;
		private readonly IProductAttributeService _productAttributeService;
		private readonly IProductApiService _productApiService;
		private readonly ICustomerApiService _customerApiService;
		private readonly IJsonHelper _jsonHelper;
		private readonly IWarehouseApiService _warehouseApiService;
		private readonly IAddressApiService _addressApiService;
		private readonly ICountryService _countryService;
		private readonly IStateProvinceService _stateProvinceService;
		private readonly IAddressService _addressService;
		private readonly IShippingService _shippingService;
		private readonly ICustomerService _customerService;
		private readonly CommonAPIManager _commonApiManager;
		private readonly IProductServiceMW _productServiceMW;



		public ApiAdminController(
			IStoreContext storeContext,
			ISettingService settingService,
			ICustomerActivityService customerActivityService,
			ILocalizationService localizationService,
			INotificationService notificationService,
			IApiHttpRequestService apiHttpRequestService,
			IUrlRecordService urlRecordService,
			IProductService productService,
			CatalogSettings catalogSettings,
			ICategoryService categoryServices,
			IManufacturerService manufacturerServices,
			IManufacturerServiceMW manufacturerServicesMW,
			ICategoryServiceMW categoryServicesMW,
			IProductAttributeService productAttributeService, IProductApiService productApiService, ICustomerApiService customerApiService,
			IJsonHelper jsonHelper, IWarehouseApiService warehouseApiServce, IAddressApiService addressApiService,
			ICountryService countryService,
			IStateProvinceService stateProvinceService,
			IAddressService addressService,
			IShippingService shippingService,
			ICustomerService customerService,
			CommonAPIManager commonApiManager,
			IProductServiceMW productServiceMW)
		{
			_storeContext = storeContext;
			_settingService = settingService;
			_customerActivityService = customerActivityService;
			_localizationService = localizationService;
			_notificationService = notificationService;
			_apiHttpRequestService = apiHttpRequestService;
			_urlRecordService = urlRecordService;
			_productService = productService;
			_catalogSettings = catalogSettings;
			_categoryServices = categoryServices;
			_manufacturerServices = manufacturerServices;
			_manufacturerServicesMW = manufacturerServicesMW;
			_categoryServicesMW = categoryServicesMW;
			_productAttributeService = productAttributeService;
			_productApiService = productApiService;
			_customerApiService = customerApiService;
			_jsonHelper = jsonHelper;
			_warehouseApiService = warehouseApiServce;
			_addressApiService = addressApiService;
			_countryService = countryService;
			_stateProvinceService = stateProvinceService;
			_addressService = addressService;
			_shippingService = shippingService;
			_customerService = customerService;
			_commonApiManager = commonApiManager;
			_productServiceMW = productServiceMW;
		}

		[HttpGet]
		public async Task<IActionResult> Settings()
		{
			var storeScope = await _storeContext.GetActiveStoreScopeConfigurationAsync();
			var apiSettings = await _settingService.LoadSettingAsync<ApiSettings>(storeScope);
			var model = apiSettings.ToModel();

			// Store Settings
			model.ActiveStoreScopeConfiguration = storeScope;

			if (model.EnableApi_OverrideForStore || storeScope == 0)
			{
				await _settingService.SaveSettingAsync(apiSettings, x => x.EnableApi, storeScope, false);
			}
			if (model.TokenExpiryInDays_OverrideForStore || storeScope == 0)
			{
				await _settingService.SaveSettingAsync(apiSettings, x => x.TokenExpiryInDays, storeScope, false);
			}

			//now clear settings cache
			await _settingService.ClearCacheAsync();

			return View($"~/Plugins/Nop.Plugin.Api/Areas/Admin/Views/ApiAdmin/Settings.cshtml", model);
		}

		[HttpPost]
		public async Task<IActionResult> Settings(ConfigurationModel model)
		{
			//load settings for a chosen store scope
			var storeScope = await _storeContext.GetActiveStoreScopeConfigurationAsync();

			var settings = model.ToEntity();

			/* We do not clear cache after each setting update.
            * This behavior can increase performance because cached settings will not be cleared 
            * and loaded from database after each update */

			if (model.EnableApi_OverrideForStore || storeScope == 0)
			{
				await _settingService.SaveSettingAsync(settings, x => x.EnableApi, storeScope, false);
			}
			if (model.TokenExpiryInDays_OverrideForStore || storeScope == 0)
			{
				await _settingService.SaveSettingAsync(settings, x => x.TokenExpiryInDays, storeScope, false);
			}

			await _settingService.SaveSettingAsync(settings, x => x.UserName, storeScope, false);
			await _settingService.SaveSettingAsync(settings, x => x.Password, storeScope, false);
			await _settingService.SaveSettingAsync(settings, x => x.GrantType, storeScope, false);
			await _settingService.SaveSettingAsync(settings, x => x.ClientId, storeScope, false);
			await _settingService.SaveSettingAsync(settings, x => x.ApiUrl, storeScope, false);
			await _settingService.SaveSettingAsync(settings, x => x.RedirectUri, storeScope, false);
			await _settingService.SaveSettingAsync(settings, x => x.PublicApiUrl, storeScope, false);
			await _settingService.SaveSettingAsync(settings, x => x.BrightPearlAppRef, storeScope, false);
			await _settingService.SaveSettingAsync(settings, x => x.BrightPearlStaffToken, storeScope, false);
			//now clear settings cache
			await _settingService.ClearCacheAsync();

			await _customerActivityService.InsertActivityAsync("EditApiSettings", "Edit Api Settings");

			_notificationService.SuccessNotification(await _localizationService.GetResourceAsync("Admin.Plugins.Saved"));

			return View($"~/Plugins/Nop.Plugin.Api/Areas/Admin/Views/ApiAdmin/Settings.cshtml", model);
		}


		#region Token


		[HttpPost, ActionName("Settings")]
		[FormValueRequired("generatetoken")]
		public async Task<IActionResult> GenerateToken(ConfigurationModel model)
		{
			var storeScope = await _storeContext.GetActiveStoreScopeConfigurationAsync();
			var settings = await _settingService.LoadSettingAsync<ApiSettings>(storeScope);
			var requestParameters = new Dictionary<string, string>
			{
				{ "grant_type", settings.GrantType },
				{ "client_id", settings.ClientId },
				{"username", settings.UserName },
				{"password", settings.Password },
				{"redirect_url", settings.RedirectUri }
			};

			var response = await _apiHttpRequestService.HttpPostRequest(requestParameters, "", "oauth/token");
			var responseModel = JsonConvert.DeserializeObject<HttpResponseToken>(response.Content.ReadAsStringAsync().Result);
			model.Token = responseModel.AccessToken;
			model.RefreshToken = responseModel.RefreshToken;
			model.ApiDomain = responseModel.ApiDomain;
			model.InstallationInstanceId = responseModel.InstallationInstanceId;
			settings = model.ToEntity();
			await _settingService.SaveSettingAsync(settings, x => x.Token, storeScope, false);
			await _settingService.SaveSettingAsync(settings, x => x.RefreshToken, storeScope, false);
			await _settingService.SaveSettingAsync(settings, x => x.ApiDomain, storeScope, false);
			await _settingService.SaveSettingAsync(settings, x => x.InstallationInstanceId, storeScope, false);
			await _settingService.ClearCacheAsync();
			return RedirectToAction("Settings");
		}

		#endregion

		#region Product, Category, Brand
		[HttpGet]
		public async Task<IActionResult> GetBrightPearlProducts()
		{
			var storeScope = await _storeContext.GetActiveStoreScopeConfigurationAsync();
			var response = await _apiHttpRequestService.HttpGetRequest(null, "product-service/product-search");

			//get products

			var model = JsonConvert.DeserializeObject<List<Response>>(response.Content.ReadAsStringAsync().Result);

			return Content("test");
		}


		//[HttpGet]
		//public async Task<List<CategoryResponse>> GetBrightPearlCategoryByCode(string Code)
		//{

		//	string url = "https://use1.brightpearlconnect.com/public-api/midwestworkwear/product-service/brightpearl-category/" + Code;
		//	var response = await _apiHttpRequestService.HttpGetRequesttest(null, url);

		//	//get products

		//	var model = JsonConvert.DeserializeObject<List<CategoryResponse>>(response.Content.ReadAsStringAsync().Result);
		//	return model;
		//}


		//[HttpGet]
		//public async Task<List<ManufacturerResponse>> GetBrightPearlManufacturerByCode(string Code)
		//{

		//	string url = "https://use1.brightpearlconnect.com/public-api/midwestworkwear/product-service/brightpearl-category/" + Code;
		//	var response = await _apiHttpRequestService.HttpGetRequesttest(null, url);

		//	//get products

		//	var model = JsonConvert.DeserializeObject<List<ManufacturerResponse>>(response.Content.ReadAsStringAsync().Result);
		//	return model;
		//}

		public async Task<IActionResult> InsertBrightPearlProductToNop(int prdId)
		{
			await _commonApiManager.InsertBrightPearlProductToNop(prdId);
			_notificationService.SuccessNotification(await _localizationService.GetResourceAsync("Admin.Plugin.API.Product.Synch.Successfully"));
			return Content("Successfully done"); // will change later


			#endregion


			//#region Customer/Contact
			//[HttpGet]
			//public async Task<IActionResult> GetCustomer()
			//{
			//	var response = await _apiHttpRequestService.HttpGetRequest(null, "contact-service/contact");
			//	var customerResponse = JToken.Parse(response.Content.ReadAsStringAsync().Result).ToObject<CustomerResponseModel.CustomerRoot>();
			//	foreach (var resItem in customerResponse.response)
			//	{
			//		var mwcId = resItem.contactId;
			//		var customer = await _customerApiService.GetCustomerByMWCId(mwcId);
			//		customer.CreatedOnUtc = resItem.createdOn;
			//		customer.LastActivityDateUtc = resItem.lastContactedOn;

			//	}



			//	return Content("test");
			//}
			//#endregion
			//#region Utilities



		}

		public async Task<IActionResult> InsertNopToBrightPearlSynch(int prdId)
		{
			var product = await _productService.GetProductByIdAsync(prdId);
            await _commonApiManager.InsertNopProductToBrightPearlAsync(product);
            _notificationService.SuccessNotification(await _localizationService.GetResourceAsync("Admin.Plugin.API.Product.Synch.Successfully"));
			return RedirectToAction("Edit", "Product", new { id = prdId });
        }

		public async Task<IActionResult> InsertBrightPearlProductToNopWithRange(int prodIdFrom, int prodIdTo)
		{
            await _commonApiManager.InsertBrightPearlProductToNopWithRange(prodIdFrom,prodIdTo);
             _notificationService.SuccessNotification(await _localizationService.GetResourceAsync("Admin.Plugin.API.Product.Synch.Successfully"));
            return Content("Success"); //Need to replace 
        }

        public async Task<IActionResult> CreateWebhook()
        {
            await _commonApiManager.CreateWebhook(null);
            return Content("test");
        }

		public async Task<IActionResult> InsertBrightPearlWarehouseToNop()
		{
			await _commonApiManager.InsertBrightPearlWarehouseToNop();
             _notificationService.SuccessNotification(await _localizationService.GetResourceAsync("Admin.Plugin.API.Warehouse.Synch.Successfully"));
			return Content("Warehouse synched Success");
        }
		public async Task<IActionResult> InserCustomer()
		{
			await _commonApiManager.GetCustomer();
            return Content("Customer synched Success");
        }

		public async Task<IActionResult> InsertCustomFieldMetadataBrightToNop()
		{
            await _commonApiManager.InsertCustomFieldMetadataBrightToNop();
            _notificationService.SuccessNotification(await _localizationService.GetResourceAsync("Admin.Plugin.API.CustomFieldMetadata.Synch.Successfully"));
            return Content("CustomFieldMetadata synched Success");
        }

        public async Task<IActionResult> InsertProductCustomFields(string prodIdsRange)
        {
            await _commonApiManager.InsertProductCustomFields(prodIdsRange);
            _notificationService.SuccessNotification(await _localizationService.GetResourceAsync("Admin.Plugin.API.ProductCustomField.Synch.Successfully"));
            return Content("CustomFieldMetadata synched Success");
        }
        
    }
}
