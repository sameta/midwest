﻿using System.Threading.Tasks;
using Nop.Services.ScheduleTasks;

namespace Nop.Plugin.Api.Areas.Admin.ScheduleTasks
{
    /// <summary>
    /// Represents a schedule task to synchronize contacts
    /// </summary>
    public class BrightPearlToNopSynchronizationTask : IScheduleTask
    {
        #region Fields

        private readonly ProductSynchManager _productSynchManager;

        #endregion

        #region Ctor

        public BrightPearlToNopSynchronizationTask(ProductSynchManager productSynchManager)
        {
            _productSynchManager = productSynchManager;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Execute task
        /// </summary>
        /// <returns>A task that represents the asynchronous operation</returns>
        public async Task ExecuteAsync()
        {
            await _productSynchManager.BrightPearlToNopSynchronizeAsync();
        }

        #endregion
    }
}