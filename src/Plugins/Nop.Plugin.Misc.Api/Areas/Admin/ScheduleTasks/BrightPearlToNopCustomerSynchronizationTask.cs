﻿using Nop.Services.ScheduleTasks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Api.Areas.Admin.ScheduleTasks
{
    public class BrightPearlToNopCustomerSynchronizationTask : IScheduleTask
    {
        private readonly CustomerSynchManager _customerSynchManager;
        public BrightPearlToNopCustomerSynchronizationTask(CustomerSynchManager customerSynchManager)
        {
            _customerSynchManager = customerSynchManager;
        }
        public async Task ExecuteAsync()
        {
            await _customerSynchManager.BrightPearlToNopSynchronizeAsync();
        }
    }
}
