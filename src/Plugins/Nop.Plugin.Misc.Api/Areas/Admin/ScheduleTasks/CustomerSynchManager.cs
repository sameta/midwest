﻿using Nop.Services.Logging;
using Nop.Core;
using Nop.Plugin.Api.Services;
using Nop.Services.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogLevel = Nop.Core.Domain.Logging.LogLevel;

namespace Nop.Plugin.Api.Areas.Admin.ScheduleTasks
{
    public class CustomerSynchManager
    {
        private readonly IStoreContext _storeContext;
        private readonly ILogger _logger;
        private readonly CommonAPIManager _commonApiManager;
        public CustomerSynchManager(IStoreContext storeContext,
            ILogger logger,
            CommonAPIManager commonApiManager)
        {
            _storeContext = storeContext;
            _logger = logger;
            _commonApiManager = commonApiManager;
        }
        public async Task<IList<(NotifyType Type, string Message)>> BrightPearlToNopSynchronizeAsync(bool synchronizationTask = true, int storeId = 0)
        {
            var messages = new List<(NotifyType, string)>();
            try
            {
                await _commonApiManager.InsertBrightCustomertoNop(1);
            }
            catch (Exception exception)
            {
                //log full error
                await _logger.InsertLogAsync(LogLevel.Error, "BrightPearl to Nop Customer Synch failed", null);
                messages.Add((NotifyType.Error, $"BrighPearl to Nop Customer synchronization error: {exception.Message}"));
            }

            return messages;
        }
    }
}
