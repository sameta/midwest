﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Nop.Core;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Shipping;
using Nop.Plugin.Api.Areas.Admin.Models.Product;
using Nop.Plugin.Api.Areas.Admin.Models.Warehouse;
using Nop.Plugin.Api.Services;
using Nop.Services.Catalog;
using Nop.Services.Messages;
using Nop.Services.ScheduleTasks;
using Nop.Services.Seo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Nop.Plugin.Api.Areas.Admin.Models.Warehouse.WarehouseResponseModel;
using Category = Nop.Core.Domain.Catalog.Category;
using Nop.Core.Domain.Logging;
using LogLevel = Nop.Core.Domain.Logging.LogLevel;
using Nop.Services.Logging;

namespace Nop.Plugin.Api.Areas.Admin.ScheduleTasks
{
    public class ProductSynchManager
    {
        private readonly IStoreContext _storeContext;
        private readonly ILogger _logger;
        private readonly CommonAPIManager _commonApiManager;
        public ProductSynchManager(IStoreContext storeContext,
            ILogger logger,
            CommonAPIManager commonApiManager)
        {
            _storeContext = storeContext;
            _logger = logger;
            _commonApiManager = commonApiManager;

        }

        public async Task<IList<(NotifyType Type, string Message)>> BrightPearlToNopSynchronizeAsync(bool synchronizationTask = true, int storeId = 0)
        {
            var messages = new List<(NotifyType, string)>();
            try
            {
                await _commonApiManager.InsertBrightPearlProductsToNop(1);
            }
            catch (Exception exception)
            {
                //log full error
                await _logger.InsertLogAsync(LogLevel.Error, "BrightPearl to Nop Product Synch failed", null);
                messages.Add((NotifyType.Error, $"BrighPearl to Nop Product synchronization error: {exception.Message}"));
            }

            return messages;
        }

        public async Task<IList<(NotifyType Type, string Message)>> NopToBrightPearlSynchronizeAsync(bool synchronizationTask = true, int storeId = 0)
        {
            var messages = new List<(NotifyType, string)>();
            try
            {
                await _commonApiManager.InsertNopProductsToBrightPearlProductsAsync();
            }
            catch (Exception exception)
            {
                //log full error
                await _logger.InsertLogAsync(LogLevel.Error, "BrightPearl to Nop Product Synch failed", null);
                messages.Add((NotifyType.Error, $"BrighPearl to Nop Product synchronization error: {exception.Message}"));
            }

            return messages;
        }
        #region Utilities


        #endregion

    }
}
