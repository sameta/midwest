﻿using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Customers;
using Nop.Plugin.Api.Services;
using Nop.Services.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Api.Areas.Admin.Services
{
    public class EventConsumer :
         IConsumer<CategoryInsertEvent>,
        IConsumer<ManufactureInsertEvent>
    {
        private readonly CommonAPIManager _commonAPIManager;
        public EventConsumer(CommonAPIManager commonAPIManager)
        {
            _commonAPIManager = commonAPIManager;
        }
        public async Task HandleEventAsync(CategoryInsertEvent categoryInsertEvent)
        {
            try
            {
                await _commonAPIManager.CheckBrightPearlCategoryExist(categoryInsertEvent.Category);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public async Task HandleEventAsync(ManufactureInsertEvent manufactureInsertEvent)
        {
            try
            {
                await _commonAPIManager.ChecktheExistingBrightPearlBrand(manufactureInsertEvent.Manufacturer);
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
