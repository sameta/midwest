﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Nop.Core;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Logging;
using Nop.Core.Infrastructure;
using Nop.Data;
using Nop.Plugin.Api.Domain;
using Nop.Services.Cms;
using Nop.Services.Configuration;
using Nop.Services.Customers;
using Nop.Services.Localization;
using Nop.Services.Plugins;
using Nop.Services.ScheduleTasks;
using Nop.Web.Framework.Infrastructure;
using Nop.Web.Framework.Menu;

namespace Nop.Plugin.Api.Infrastructure
{
    public class ApiPlugin : BasePlugin, IWidgetPlugin
    {
        private readonly ICustomerService _customerService;
        private readonly ILocalizationService _localizationService;
        private readonly ISettingService _settingService;
        private readonly IWebHelper _webHelper;
        private readonly IWorkContext _workContext;
        private readonly IScheduleTaskService _scheduleTaskService;

        public ApiPlugin(
            ISettingService settingService,
            IWorkContext workContext,
            ICustomerService customerService,
            ILocalizationService localizationService,
            IWebHelper webHelper,
            IScheduleTaskService scheduleTaskService)
        {
            _settingService = settingService;
            _workContext = workContext;
            _customerService = customerService;
            _localizationService = localizationService;
            _webHelper = webHelper;
            _scheduleTaskService = scheduleTaskService;
        }

        bool IWidgetPlugin.HideInWidgetList => true;
        public override async Task InstallAsync()
        {
            //locales

            await _localizationService.AddOrUpdateLocaleResourceAsync(new Dictionary<string, string>
            {
                { "Plugin.Api.BrightPearl.Field.CustomerRoles", "Customer Roles" },
                {"Plugin.Api.BrightPearl.Field.Username", "UserName" },
                {"Plugin.Api.BrightPearl.Field.Password", "Password"},
                {"Plugin.Api.BrightPearl.Field.ClientId","Client Id"},
                {"Plugin.Api.BrightPearl.Field.ClientSecret", "Key"},
                {"Plugin.Api.BrightPearl.Field.RedirectUri","Redirect Url"},
                {"Plugins.Api.BrightPearl.Configuration", "Bright Pearl Configuration"},
                {"Plugin.Api.BrightPearl.Field.Token","Token"},
                {"Plugin.Api.BrightPearl.Field.GenerateToken","Generate Token"},
                {"Plugins.Api.Admin.EnableApi", "EnableApi" },
                {"Plugins.Api.Admin.TokenExpiryInDays", "Token Expiry In Days" },
                {"Plugin.Api.BrightPearl.Field.GrantType", "Grant Type" },
                {"Plugin.Api.BrightPearl.Field.ApiUrl", "Api Url" },
                {"Plugin.Api.BrightPearl.Field.PublicApiUrl", "Public Api Url" },
                {"Plugin.Api.BrightPearl.Field.BrightPearlAppRef", "Bright Pearl App Ref" },
                {"Plugin.Api.BrightPearl.Field.BrightPearlStaffToken", "Bright Pearl Staff Token" },

            });



            await _settingService.SaveSettingAsync(new ApiSettings
            {
                ApiDomain = "use1.brightpearlconnect.com",
                EnableApi = true,
                GrantType = "password",
                ClientId = "sigmasolve",
                ApiUrl = "https://use1.brightpearlconnect.com/midwestworkwear",
                RedirectUri = "https://brightpearl.sigmasolve.com/index.php",
                Token = "gEPQ6e+hkm/D4hlT4kh/kGZAqESMkLXACTGsD+Y2Zt4=",
                RefreshToken = "8sTmeU8IVOU2GIT2jRc4Q04+Du4MQhj/nZtp38MHyJ8=",
                BrightPearlAppRef = "midwestworkwear_brightpearldev",
                BrightPearlStaffToken = "kCZxSqi3Apr9QfjJqfIjrARgPYYXS+73+qYQaUoqLJ0=",
                PublicApiUrl = "https://use1.brightpearlconnect.com/public-api/midwestworkwear",
                UserName = "nshah@sigmasolve.net",
                Password = "Sigma@@1122"
            });

            await _scheduleTaskService.InsertTaskAsync(new()
            {
                Enabled = true,
                LastEnabledUtc = DateTime.UtcNow,
                Seconds = 86000,
                Name = "BrightPearlToNopProductSynch",
                Type = "Nop.Plugin.Api.Areas.Admin.ScheduleTasks.BrightPearlToNopSynchronizationTask"
            });
            await _scheduleTaskService.InsertTaskAsync(new()
            {
                Enabled = true,
                LastEnabledUtc = DateTime.UtcNow,
                Seconds = 86000,
                Name = "NopToBrightPearlProductSynch",
                Type = "Nop.Plugin.Api.Areas.Admin.ScheduleTasks.NopToBrightPearlSynchronizationTask"
            });

            var apiRole = await _customerService.GetCustomerRoleBySystemNameAsync(Constants.Roles.ApiRoleSystemName);

            if (apiRole == null)
            {
                apiRole = new CustomerRole
                {
                    Name = Constants.Roles.ApiRoleName,
                    Active = true,
                    SystemName = Constants.Roles.ApiRoleSystemName
                };

                await _customerService.InsertCustomerRoleAsync(apiRole);
            }
            else if (apiRole.Active == false)
            {
                apiRole.Active = true;
                await _customerService.UpdateCustomerRoleAsync(apiRole);
            }

            var activityLogTypeRepository = EngineContext.Current.Resolve<IRepository<ActivityLogType>>();
            var activityLogType = (await activityLogTypeRepository.GetAllAsync(query =>
            {
                return query.Where(x => x.SystemKeyword == "Api.TokenRequest");
            })).FirstOrDefault();

            if (activityLogType == null)
            {
                await activityLogTypeRepository.InsertAsync(new ActivityLogType
                {
                    SystemKeyword = "Api.TokenRequest",
                    Name = "API token request",
                    Enabled = true
                });
            }

            await base.InstallAsync();

            // Changes to Web.Config trigger application restart.
            // This doesn't appear to affect the Install function, but just to be safe we will made web.config changes after the plugin was installed.
            //_webConfigMangerHelper.AddConfiguration();
        }

        public override async Task UninstallAsync()
        {
            //locales
            await _localizationService.DeleteLocaleResourceAsync("Plugins.Api");

            //_localizationService.DeletePluginLocaleResource("Plugins.Api.Admin.Menu.Title");
            //_localizationService.DeletePluginLocaleResource("Plugins.Api.Admin.Menu.Settings.Title");

            //_localizationService.DeletePluginLocaleResource("Plugins.Api.Admin.Configure");
            //_localizationService.DeletePluginLocaleResource("Plugins.Api.Admin.GeneralSettings");
            //_localizationService.DeletePluginLocaleResource("Plugins.Api.Admin.EnableApi");
            //_localizationService.DeletePluginLocaleResource("Plugins.Api.Admin.EnableApi.Hint");

            //_localizationService.DeletePluginLocaleResource("Plugins.Api.Admin.Settings.GeneralSettingsTitle");
            //_localizationService.DeletePluginLocaleResource("Plugins.Api.Admin.Edit");


            var apiRole = await _customerService.GetCustomerRoleBySystemNameAsync(Constants.Roles.ApiRoleSystemName);
            if (apiRole != null)
            {
                apiRole.Active = false;
                await _customerService.UpdateCustomerRoleAsync(apiRole);
            }

            var task = await _scheduleTaskService.GetTaskByTypeAsync("BrightPearlToNopProduct");
            if (task is not null)
                await _scheduleTaskService.DeleteTaskAsync(task);

            var activityLogTypeRepository = EngineContext.Current.Resolve<IRepository<ActivityLogType>>();
            var activityLogType = (await activityLogTypeRepository.GetAllAsync(query =>
            {
                return query.Where(x => x.SystemKeyword.Equals("Api.TokenRequest"));
            })).FirstOrDefault();
            if (activityLogType != null)
            {
                activityLogType.Enabled = false;
                await activityLogTypeRepository.UpdateAsync(activityLogType);
            }

            await base.UninstallAsync();
        }

        public override string GetConfigurationPageUrl()
        {
            return $"{_webHelper.GetStoreLocation()}Admin/ApiAdmin/Settings";
        }

        public async Task ManageSiteMapAsync(SiteMapNode rootNode)
        {
            var workingLanguage = await _workContext.GetWorkingLanguageAsync();

            var pluginMenuName = await _localizationService.GetResourceAsync("Plugins.Api.Admin.Menu.Title", workingLanguage.Id, defaultValue: "API");

            var settingsMenuName = await _localizationService.GetResourceAsync("Plugins.Api.Admin.Menu.Settings.Title", workingLanguage.Id, defaultValue: "API");

            const string adminUrlPart = "Admin/";

            var pluginMainMenu = new SiteMapNode
            {
                Title = pluginMenuName,
                Visible = true,
                SystemName = "Api-Main-Menu",
                IconClass = "fa-genderless"
            };

            pluginMainMenu.ChildNodes.Add(new SiteMapNode
            {
                Title = settingsMenuName,
                Url = _webHelper.GetStoreLocation() + adminUrlPart + "ApiAdmin/Settings",
                Visible = true,
                SystemName = "Api-Settings-Menu",
                IconClass = "fa-genderless"
            });


            rootNode.ChildNodes.Add(pluginMainMenu);
        }

        public Type GetWidgetViewComponent(string widgetZone)
        {
            //if (widgetZone.ToUpper() == AdminWidgetZones.ProductDetailsButtons.ToUpper())
            //{
            //    return "ApiProductSynch";
            //}
            return null;
        }
        public async Task<IList<string>> GetWidgetZonesAsync()
        {
            return await Task.FromResult<IList<string>>(new List<string>
            {
                AdminWidgetZones.ProductDetailsButtons
            });
        }
    }
}
