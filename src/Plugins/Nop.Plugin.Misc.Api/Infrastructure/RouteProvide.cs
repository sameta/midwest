﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Routing;
using Nop.Services.Installation;
using Nop.Web.Framework;
using Nop.Web.Framework.Mvc.Routing;
using Nop.Web.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Api.Infrastructure
{
    public class RouteProvide : IRouteProvider
    {
        #region Methods

        /// <summary>
        /// Register routes
        /// </summary>
        /// <param name="endpointRouteBuilder">Route builder</param>
        public void RegisterRoutes(IEndpointRouteBuilder endpointRouteBuilder)
        {
            //get language pattern
            //it's not needed to use language pattern in AJAX requests and for actions returning the result directly (e.g. file to download),
            //use it only for URLs of pages that the user can go to

            endpointRouteBuilder.MapControllerRoute(name: "Plugin.Api.InsertBrightPearlProductToNop",
                pattern: $"Product/ProductSynch/{{prdId:min(0)}}",
                defaults: new { controller = "ApiAdmin", action = "InsertBrightPearlProductToNop", area = AreaNames.Admin });

            endpointRouteBuilder.MapControllerRoute(name: "Plugin.Api.InsertBrightPearlToNop",
               pattern: $"Product/ProductRangeSynch/{{prodIdFrom:min(0)}}/{{prodIdTo:min(0)}}",
               defaults: new { controller = "ApiAdmin", action = "InsertBrightPearlProductToNopWithRange", area = AreaNames.Admin });
            
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets a priority of route provider
        /// </summary>
        public int Priority => -1;

        #endregion
    }
}
