﻿using Nop.Core.Configuration;

namespace Nop.Plugin.Api.Domain
{
    public class ApiSettings : ISettings
    {
        public bool EnableApi { get; set; } = true;

        public string UserName { get; set; }

        public string Password { get; set; }

        public string RedirectUri { get; set; }

        public string ClientId { get; set; }

        public string ClientSecret { get; set; }

        public string ApiUrl { get; set; }

        public string Token { get; set; }

        public string RefreshToken { get; set; }

        public string ExpirationTime { get; set; }

        public string TokenType { get; set; }

        public string ApiDomain { get; set;}

        public string InstallationInstanceId { get; set; }

        public string GrantType { get; set; }

        // these two properties are required to get any data from api like product ,category etc
        public string BrightPearlAppRef { get; set; }

        public string BrightPearlStaffToken { get; set; }

        public string PublicApiUrl { get; set; }
        public int TokenExpiryInDays { get; set; } = 0;
    }
}
