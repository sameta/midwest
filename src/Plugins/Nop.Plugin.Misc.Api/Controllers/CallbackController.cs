﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Nop.Plugin.Api.Factories;
using Nop.Plugin.Api.Helpers;
using Nop.Plugin.Api.JSON.Serializers;
using Nop.Plugin.Api.Models.Webhook.Requests;
using Nop.Plugin.Api.Services;
using Nop.Services.Catalog;
using Nop.Services.Customers;
using Nop.Services.Discounts;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Media;
using Nop.Services.Security;
using Nop.Services.Seo;
using Nop.Services.Stores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Api.Controllers
{
    [AllowAnonymous]
    public class CallbackController : BaseApiController
    {
        private readonly CommonAPIManager _commonApiManager;
        private readonly ILogger _logger;
        public CallbackController(IJsonFieldsSerializer jsonFieldsSerializer,
            ICustomerActivityService customerActivityService,
            ILocalizationService localizationService,
            IPictureService pictureService,
            IStoreMappingService storeMappingService,
            IStoreService storeService,
            IDiscountService discountService,
            IAclService aclService,
            ICustomerService customerService, CommonAPIManager commonApiManager, ILogger logger
            ) : base(jsonFieldsSerializer, aclService, customerService, storeMappingService, storeService, discountService,
                                         customerActivityService, localizationService, pictureService)
        {
            _commonApiManager = commonApiManager;
            _logger = logger;
        }

        [Route("/api/webhook/callback", Name = "Callback")]
        public virtual async Task<IActionResult> CallBackWebhook(WebhookBodyTemplate model)
        {
            await _logger.InsertLogAsync(Core.Domain.Logging.LogLevel.Information, "Webhook callback");
            switch (model.resourceType.ToLower())
            {
                case "product":
                    if (Convert.ToInt32(model.id) > 0)
                    {
                        await _logger.InsertLogAsync(Core.Domain.Logging.LogLevel.Information, "ready to insert product into nop");
                        await _commonApiManager.InsertBrightPearlProductToNop(Convert.ToInt32(model.id));
                    }
                        break;
                default:
                    break;
            }

            return new JsonResult(new object());
        }
    }
}
