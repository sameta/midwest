﻿using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.IO;

namespace Nop.Plugin.Api.Helpers
{
    public interface IJsonHelper
    {
        Dictionary<string, object> GetRequestJsonDictionaryFromStream(Stream stream, bool rewindStream);
        string GetRootPropertyName<T>() where T : class, new();

        Dictionary<string, object> ReadJsonDynamicProperty(JObject jObject,string rootProp, string propName = "", string firstPropName = "");

        Dictionary<string, Dictionary<string, string>> CreateObjectFromNestedJson(Dictionary<string, object> keyValueCollection);

        Dictionary<string, List<string>> CreateListFromNestedJson(Dictionary<string, object> keyValueCollection);

        Dictionary<string, Dictionary<string, string>> CreateTierPriceListByCustomerRole(Dictionary<string, object> keyValueCollection);

        Dictionary<string, object> ReadJobjectProperty(JObject jObject, string rootProp, string propName = "", string firstPropName = "");
    }
}
