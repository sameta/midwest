﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.Json.Nodes;
using Newtonsoft.Json.Linq;
using Nop.Services.Localization;

namespace Nop.Plugin.Api.Helpers
{
    public class JsonHelper : IJsonHelper
    {
        #region Constructors

        public JsonHelper(ILanguageService languageService, ILocalizationService localizationService)
        {
            _localizationService = localizationService;
        }

        #endregion

        #region Private Fields

        private readonly ILocalizationService _localizationService;

        #endregion

        #region Public Methods

        public Dictionary<string, object> GetRequestJsonDictionaryFromStream(Stream stream, bool rewindStream)
        {
            var json = GetRequestBodyString(stream, rewindStream);
            if (string.IsNullOrEmpty(json))
            {
                throw new InvalidOperationException("No Json provided");
            }

            var requestBodyDictionary = DeserializeToDictionary(json);
            if (requestBodyDictionary == null || requestBodyDictionary.Count == 0)
            {
                throw new InvalidOperationException("Json format is invalid");
            }

            return requestBodyDictionary;
        }

        public string GetRootPropertyName<T>() where T : class, new()
        {
            var rootProperty = "";

            var jsonObjectAttribute = ReflectionHelper.GetJsonObjectAttribute(typeof(T));
            if (jsonObjectAttribute != null)
            {
                rootProperty = jsonObjectAttribute.Title;
            }

            if (string.IsNullOrEmpty(rootProperty))
            {
                throw new InvalidOperationException($"Error getting root property for type {typeof(T).FullName}.");
            }

            return rootProperty;
        }

        public Dictionary<string, object> ReadJsonDynamicProperty(JObject jObject, string rootProp, string propName = "", string firstPropName = "")

        {
            if (jObject != null)
            {
                JArray jAr = (JArray)jObject[rootProp];
                Dictionary<string, object> dictionary = new Dictionary<string, object>();
                //var files = JArray.Parse(YourJSON);
                foreach (JToken item in jAr.Children())
                {
                    var id = "";
                    foreach (JProperty prop in item.Children<JProperty>())
                    {
                        if (!string.IsNullOrEmpty(firstPropName) && prop.Name.Equals(firstPropName))
                            id = prop.Value.ToString();
                        if (!string.IsNullOrEmpty(propName) && prop.Name.Equals(propName))
                        {
                            string propConcat = "";
                            if (!string.IsNullOrEmpty(id))
                                //propConcat = string.Format("{0}_{1}", prop.Name.ToString(), id);
                                propConcat = id;
                            else
                                propConcat = prop.Name.ToString();
                            dictionary.Add(propConcat, prop.Value);
                        }

                    }
                }

                return dictionary;
            }
            return new Dictionary<string, object>();
        }

        public Dictionary<string, object> ReadJobjectProperty(JObject jObject, string rootProp, string propName = "", string firstPropName = "")

        {
            if (jObject != null)
            {
                var jObj = jObject[rootProp];
                Dictionary<string, object> dictionary = new Dictionary<string, object>();
                //var files = JArray.Parse(YourJSON);
                if (jObj.Children<JProperty>().Count() > 0)
                {
                    foreach (JProperty prop in jObj.Children<JProperty>())
                    {
                        dictionary.Add(prop.Name, prop.Value);
                        //var id = "";
                        //if (!string.IsNullOrEmpty(firstPropName) && prop.Name.Equals(firstPropName))
                        //    id = prop.Value.ToString();
                        //if (!string.IsNullOrEmpty(propName) && prop.Name.Equals(propName))
                        //{
                        //    string propConcat = "";
                        //    if (!string.IsNullOrEmpty(id))
                        //        //propConcat = string.Format("{0}_{1}", prop.Name.ToString(), id);
                        //        propConcat = id;
                        //    else
                        //        propConcat = prop.Name.ToString();
                        //    dictionary.Add(propConcat, prop.Value);
                        //}
                    }
                }
                return dictionary;
            }
            return new Dictionary<string, object>();
        }
        public Dictionary<string, Dictionary<string, string>> CreateObjectFromNestedJson(Dictionary<string, object> keyValueCollection)
        {
            Dictionary<string, Dictionary<string, string>> dataCollectionDicList = new Dictionary<string, Dictionary<string, string>>();
            foreach (var keyValue in keyValueCollection)
            {
                Dictionary<string, string> dataCollectionDic = new Dictionary<string, string>();
                JObject jObj = JObject.Parse(keyValue.Value.ToString());
                foreach (JProperty prp in jObj.Properties())
                {
                    dataCollectionDic.Add("Id", prp.Name.ToString());
                    foreach (JProperty wProperty in jObj[prp.Name].Children<JProperty>())
                    {
                        dataCollectionDic.Add(wProperty.Name.ToString(), wProperty.Value.ToString());
                    }
                    dataCollectionDicList.Add(keyValue.Key.ToString(), dataCollectionDic);
                }
            }
            return dataCollectionDicList;
        }

        public Dictionary<string, List<string>> CreateListFromNestedJson(Dictionary<string, object> keyValueCollection)
        {
            Dictionary<string, List<string>> dataCollectionDicList = new Dictionary<string, List<string>>();
            foreach (var keyValue in keyValueCollection)
            {
                List<string> dataCollectionDic = new List<string>();
                JObject jObj = JObject.Parse(keyValue.Value.ToString());
                foreach (JProperty prp in jObj.Properties())
                {
                    dataCollectionDic.Add(prp.Value.ToString());

                }
                dataCollectionDicList.Add(keyValue.Key.ToString(), dataCollectionDic);
            }
            return dataCollectionDicList;
        }
        public Dictionary<string, Dictionary<string, string>> CreateTierPriceListByCustomerRole(Dictionary<string, object> keyValueCollection)
        {

            Dictionary<string, Dictionary<string, string>> dataCollectionList = new Dictionary<string, Dictionary<string, string>>();
            foreach (var keyValue in keyValueCollection)
            {
                Dictionary<string, string> quantityDic = new Dictionary<string, string>();
                JObject jObj = JObject.Parse(keyValue.Value.ToString());
                foreach (JProperty prp in jObj.Properties())
                {
                    quantityDic.Add(prp.Name.ToString(), prp.Value.ToString());
                }
                dataCollectionList.Add(keyValue.Key.ToString(), quantityDic);
            }
            return dataCollectionList;
        }
        #endregion

        #region Private Methods

        // source - http://stackoverflow.com/questions/5546142/how-do-i-use-json-net-to-deserialize-into-nested-recursive-dictionary-and-list
        private Dictionary<string, object> DeserializeToDictionary(string json)
        {
            //TODO: JToken.Parse could throw an exeption if not valid JSON string is passed
            try
            {
                return ToObject(JToken.Parse(json)) as Dictionary<string, object>;
            }
            catch (Exception)
            {
                return null;
            }
        }

        // source - http://stackoverflow.com/questions/5546142/how-do-i-use-json-net-to-deserialize-into-nested-recursive-dictionary-and-list
        private object ToObject(JToken token)
        {
            switch (token.Type)
            {
                case JTokenType.Object:
                    return token.Children<JProperty>()
                                .ToDictionary(prop => prop.Name,
                                              prop => ToObject(prop.Value));

                case JTokenType.Array:
                    return token.Select(ToObject).ToList();

                default:
                    return ((JValue)token).Value;
            }
        }

        private string GetRequestBodyString(Stream stream, bool rewindStream)
        {
            var result = "";

            using (var streamReader = new StreamReader(stream, Encoding.UTF8, true, 1024, rewindStream))
            {
                result = streamReader.ReadToEnd();
                if (rewindStream)
                {
                    stream.Position = 0; //reset position to allow reading again later
                }
            }

            return result;
        }

        #endregion
    }
}
