﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Api.Services
{
    public interface ICustomLogger
    {
        Task LogWrite(string logType,string message);
    }
}
