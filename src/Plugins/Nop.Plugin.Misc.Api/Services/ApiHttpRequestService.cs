﻿using DocumentFormat.OpenXml.ExtendedProperties;
using DocumentFormat.OpenXml.Spreadsheet;
using DocumentFormat.OpenXml.Wordprocessing;
using Microsoft.VisualBasic;
using Newtonsoft.Json;
using Nop.Core;
using Nop.Plugin.Api.Areas.Admin.Models;
using Nop.Plugin.Api.Domain;
using Nop.Services.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Json;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Api.Services
{
    public class ApiHttpRequestService : IApiHttpRequestService
    {
        private readonly ISettingService _settingService;
        private readonly IStoreContext _storeContext;

        public ApiHttpRequestService(ISettingService settingService, IStoreContext storeContext)
        {
            _settingService = settingService;
            _storeContext = storeContext;
        }

        // to generate oAuth token only
        public async Task<HttpResponseMessage> HttpPostRequest(Dictionary<string, string> parameters, string token, string action)
        {
           // var storeScope = await _storeContext.GetActiveStoreScopeConfigurationAsync();
            var settings = await _settingService.LoadSettingAsync<ApiSettings>();
            var url = string.Format("{0}/{1}", settings.ApiUrl, action);
            //var authToken = settings.Token;
            //var brightPearlAppRef = settings.BrightPearlAppRef;
            //var brightStafftoken = settings.BrightPearlStaffToken;
            //var request = new HttpRequestMessage(HttpMethod.Post, url);
            //request.Headers.Add("Authorization", "Bearer " + authToken);
            //request.Headers.Add("brightpearl-app-ref", brightPearlAppRef);
            //request.Headers.Add("brightpearl-staff-token", brightStafftoken);
            HttpClient client = new HttpClient();
            var encodedContent = new FormUrlEncodedContent(parameters);
            var response = await client.PostAsync(url, encodedContent);
            return response;
        }

        public async Task<HttpResponseMessage> HttpPostRequest(string parameters, string action)
        {
            //var storeScope = await _storeContext.GetActiveStoreScopeConfigurationAsync();
            var settings = await _settingService.LoadSettingAsync<ApiSettings>();
            var url = string.Format("{0}/{1}", settings.PublicApiUrl, action);
            var authToken = settings.Token;
            var brightPearlAppRef = settings.BrightPearlAppRef;
            var brightStafftoken = settings.BrightPearlStaffToken;
            var request = new HttpRequestMessage(HttpMethod.Post, url);
            request.Content = new StringContent(parameters, Encoding.UTF8, "application/json");
            request.Headers.Add("Authorization", "Bearer " + authToken);
            request.Headers.Add("brightpearl-app-ref", brightPearlAppRef);
            request.Headers.Add("brightpearl-staff-token", brightStafftoken);
            HttpClient client = new HttpClient();
            //var requestContent = new StringContent(parameters, Encoding.UTF8, "application/json");
            var response = await client.SendAsync(request);
            return response;
        }

        public async Task<HttpResponseMessage> HttpGetRequest(IDictionary<string, string> parameters, string action)
        {
           // var storeScope = await _storeContext.GetActiveStoreScopeConfigurationAsync();
            var settings = await _settingService.LoadSettingAsync<ApiSettings>();
            var url = string.Format("{0}/{1}", settings.PublicApiUrl, action);
            var authToken = settings.Token;
            var brightPearlAppRef = settings.BrightPearlAppRef;
            var brightStafftoken = settings.BrightPearlStaffToken;
            var request = new HttpRequestMessage(HttpMethod.Get, url);
            request.Headers.Add("Authorization", "Bearer " + authToken);
            request.Headers.Add("brightpearl-app-ref", brightPearlAppRef);
            request.Headers.Add("brightpearl-staff-token", brightStafftoken);
            HttpClient client = new HttpClient();
            //var encodedContent = new FormUrlEncodedContent(parameters);
            var response = await client.SendAsync(request);
            return response;

        }
        public async Task<HttpResponseMessage> HttpPutRequest(string parameters, string action)
        {
            //var storeScope = await _storeContext.GetActiveStoreScopeConfigurationAsync();
            var settings = await _settingService.LoadSettingAsync<ApiSettings>();
            var url = string.Format("{0}/{1}", settings.ApiUrl, action);
            var authToken = settings.Token;
            var brightPearlAppRef = settings.BrightPearlAppRef;
            var brightStafftoken = settings.BrightPearlStaffToken;
            var request = new HttpRequestMessage(HttpMethod.Put, url);
            request.Content = new StringContent(parameters, Encoding.UTF8, "application/json");
            request.Headers.Add("Authorization", "Bearer " + authToken);
            request.Headers.Add("brightpearl-app-ref", brightPearlAppRef);
            request.Headers.Add("brightpearl-staff-token", brightStafftoken);
            HttpClient client = new HttpClient();
            //var requestContent = new StringContent(parameters, Encoding.UTF8, "application/json");
            var response = await client.SendAsync(request);
            return response;
        }

        //public async Task<HttpResponseMessage> HttpGetRequesttest(IDictionary<string, string> parameters, string action)
        //{
        //    var settings = await _settingService.LoadSettingAsync<ApiSettings>();
        //    var url = action;
        //    var authToken = "ZvtkFVhB83RlvtDIikkdzXiaEN/NJ/ii9sJTi7RvFBg=";
        //    var brightPearlAppRef = "midwestworkwear_brightpearldev";
        //    var brightStafftoken = "kCZxSqi3Apr9QfjJqfIjrARgPYYXS+73+qYQaUoqLJ0=";
        //    var request = new HttpRequestMessage(HttpMethod.Get, url);
        //    request.Headers.Add("Authorization", "Bearer " + authToken);
        //    request.Headers.Add("brightpearl-app-ref", brightPearlAppRef);
        //    request.Headers.Add("brightpearl-staff-token", brightStafftoken);
        //    HttpClient client = new HttpClient();
        //    //var encodedContent = new FormUrlEncodedContent(parameters);
        //    var response = await client.SendAsync(request);
        //    return response;

        //}

    }
}
