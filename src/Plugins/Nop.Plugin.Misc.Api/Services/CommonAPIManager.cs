﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using Nop.Plugin.Api.Areas.Admin.Models.Category;
using Nop.Plugin.Api.Areas.Admin.Models.Manufacturer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Core.Domain.Customers;
using Nop.Plugin.Api.Areas.Admin.Models.Customer;
using Nop.Services.Logging;
using Nop.Services.Localization;
using Nop.Services.Configuration;
using Nop.Core;
using Nop.Services.Seo;
using Nop.Services.Catalog;
using Nop.Core.Domain.Catalog;
using Nop.Plugin.Api.Helpers;
using Nop.Services.Directory;
using Nop.Services.Common;
using Nop.Services.Shipping;
using Nop.Services.Customers;
using Nop.Plugin.Api.Areas.Admin.Models.Product;
using Nop.Core.Domain.Shipping;
using Nop.Plugin.Api.Areas.Admin.Models.Warehouse;
using Category = Nop.Core.Domain.Catalog.Category;
using static Nop.Plugin.Api.Areas.Admin.Models.Warehouse.WarehouseResponseModel;
using static Nop.Plugin.Api.Areas.Admin.Models.Product.ProductSearchResponseModel;
using Nop.Plugin.Api.AutoMapper;
using Nop.Web.Areas.Admin.Models.Catalog;
using Microsoft.AspNetCore.Mvc.TagHelpers;
using Nop.Plugin.Api.Areas.Admin.Models.Manufacturer.Requests;
using Nop.Plugin.Api.Areas.Admin.Models.Manufacturer.Responses;
using Nop.Plugin.Api.Areas.Admin.Models.Options.Requests;
using Nop.Plugin.Api.Areas.Admin.Models.Options.Responses;
using static Nop.Plugin.Api.Areas.Admin.Models.Product.ProductRequestModel;
using Nop.Plugin.Api.Domain;
using System.Net.Http.Headers;
using System.Net.Http;
using static Nop.Plugin.Api.Areas.Admin.Models.TirePrice.TirePriceModel;
using CategoryResponse = Nop.Plugin.Api.Areas.Admin.Models.Category.Responses;
using CategoryOuterResponse = Nop.Plugin.Api.Areas.Admin.Models.Category;
using Nop.Plugin.Api.Areas.Admin.Models.Category.Requests;
using Nop.Plugin.Api.Areas.Admin.Models.Pricing.Responses;
using Nop.Web.Framework.Mvc.Filters;
using Nop.Web.Framework;
using NUglify.Helpers;
using Microsoft.AspNetCore.Mvc.Rendering;
using IJsonHelper = Nop.Plugin.Api.Helpers.IJsonHelper;
using Nop.Plugin.Api.Areas.Admin.Models.Common;
using Nop.Plugin.Api.Areas.Admin.Models.Product.Responses;
using Nop.Plugin.Api.Models.Webhook.Requests;
using static Nop.Plugin.Api.Areas.Admin.Models.Product.ProductResponseModel;
using Nop.Core.Domain.Messages;
using Nop.Core.Domain.Stores;
using Nop.Services.Messages;
using System.Text.RegularExpressions;
using System.Timers;
using static Nop.Plugin.Api.Areas.Admin.Models.Customer.CustomerResponseModel;
using DocumentFormat.OpenXml.Office2010.Excel;
using Nop.Plugin.Api.Areas.Admin.Models.CustomFields.Responses;
using Microsoft.AspNetCore.Http;
using Nop.Services.Logging;
using Irony.Parsing;

namespace Nop.Plugin.Api.Services
{
    //[AuthorizeAdmin]
    //[Area(AreaNames.Admin)]
    //[AutoValidateAntiforgeryToken]
    public class CommonAPIManager
    {
        private readonly ICustomerActivityService _customerActivityService;
        private readonly ILocalizationService _localizationService;
        private readonly ISettingService _settingService;
        private readonly IStoreContext _storeContext;
        private readonly IApiHttpRequestService _apiHttpRequestService;

        private readonly IUrlRecordService _urlRecordService;

        private readonly IProductService _productService;

        private readonly CatalogSettings _catalogSettings;

        private readonly ICategoryService _categoryServices;
        private readonly IManufacturerService _manufacturerServices;
        private readonly IManufacturerServiceMW _manufacturerServicesMW;
        private readonly ICategoryServiceMW _categoryServicesMW;
        private readonly IProductAttributeService _productAttributeService;
        private readonly IProductApiService _productApiService;
        private readonly ICustomerApiService _customerApiService;
        private readonly IJsonHelper _jsonHelper;
        private readonly IWarehouseApiService _warehouseApiService;
        private readonly IAddressApiService _addressApiService;
        private readonly ICountryService _countryService;
        private readonly IStateProvinceService _stateProvinceService;
        private readonly IAddressService _addressService;
        private readonly IShippingService _shippingService;
        private readonly ICustomerService _customerService;
        private readonly ILogger _logger;
        private readonly ICustomLogger _customLogger;
        protected const string ContentType = "application/json";
        private readonly HttpClient _httpClient;
        private readonly IProductServiceMW _productServiceMW;
        private readonly IAddressMWService _addressMWService;
        private readonly INewsLetterSubscriptionService _newsLetterSubscriptionService;
        private readonly IContactHistoryServices _contactHistoryServices;
        private readonly ICustomerMWService _customerMWService;
        private readonly ITaxCodeServices _taxCodeServices;
        private readonly INominalCodeServices _nominalCodeServices;
        private readonly IProductAttributesApiService _productAttributesApiService;
        private readonly ISpecificationAttributeApiService _specificationAttributeApiService;
        private readonly ISpecificationAttributeService _specificationService;

        public CommonAPIManager(
            IStoreContext storeContext,
            ISettingService settingService,
            ICustomerActivityService customerActivityService,
            ILocalizationService localizationService,
            IApiHttpRequestService apiHttpRequestService,
            IUrlRecordService urlRecordService,
            IProductService productService,
            CatalogSettings catalogSettings,
            ICategoryService categoryServices,
            IManufacturerService manufacturerServices,
            IManufacturerServiceMW manufacturerServicesMW,
            ICategoryServiceMW categoryServicesMW,
            IProductAttributeService productAttributeService, IProductApiService productApiService, ICustomerApiService customerApiService,
            IJsonHelper jsonHelper, IWarehouseApiService warehouseApiServce, IAddressApiService addressApiService,
            ICountryService countryService,
            IStateProvinceService stateProvinceService,
            IAddressService addressService,
            IShippingService shippingService,
            ICustomerService customerService, ILogger logger, ICustomLogger customLogger,
            HttpClient httpClient, IProductServiceMW productServiceMW
            , IAddressMWService addressMWService,
            INewsLetterSubscriptionService newsLetterSubscriptionService, IContactHistoryServices contactHistoryServices,
            ICustomerMWService customerMWService, ITaxCodeServices taxCodeServices, INominalCodeServices nominalCodeServices,
            IProductAttributesApiService productAttributesApiService,
            ISpecificationAttributeApiService specificationAttributeApiService,
            ISpecificationAttributeService specificationService)
        {
            _storeContext = storeContext;
            _settingService = settingService;
            _customerActivityService = customerActivityService;
            _localizationService = localizationService;
            _apiHttpRequestService = apiHttpRequestService;
            _urlRecordService = urlRecordService;
            _productService = productService;
            _catalogSettings = catalogSettings;
            _categoryServices = categoryServices;
            _manufacturerServices = manufacturerServices;
            _manufacturerServicesMW = manufacturerServicesMW;
            _categoryServicesMW = categoryServicesMW;
            _productAttributeService = productAttributeService;
            _productApiService = productApiService;
            _customerApiService = customerApiService;
            _jsonHelper = jsonHelper;
            _warehouseApiService = warehouseApiServce;
            _addressApiService = addressApiService;
            _countryService = countryService;
            _stateProvinceService = stateProvinceService;
            _addressService = addressService;
            _shippingService = shippingService;
            _customerService = customerService;
            _logger = logger;
            _customLogger = customLogger;
            _httpClient = httpClient;
            _productServiceMW = productServiceMW;
            _addressMWService = addressMWService;
            _newsLetterSubscriptionService = newsLetterSubscriptionService;
            _contactHistoryServices = contactHistoryServices;
            _customerMWService = customerMWService;
            _taxCodeServices = taxCodeServices;
            _nominalCodeServices = nominalCodeServices;
            _productAttributesApiService = productAttributesApiService;
            _specificationAttributeApiService = specificationAttributeApiService;
            _specificationService = specificationService;
        }

        string[] staticYes_NoOptions = {"false", "true", "not set"};
        #region



        [HttpGet]
        public async Task<ManufacturerResponseModel.ManufacturerRoot> GetBrightPearlManufacturerByCode(string Code)
        {
            await _logger.InsertLogAsync(Core.Domain.Logging.LogLevel.Information, "Call to Get BrightPearl Manufactorer ");
            var response = await _apiHttpRequestService.HttpGetRequest(null, "product-service/brand/" + Code);
            //string url = "https://use1.brightpearlconnect.com/public-api/midwestworkwear/product-service/brightpearl-category/" + Code;
            //var response = await _apiHttpRequestService.HttpGetRequesttest(null, url);

            //get products

            var model = JsonConvert.DeserializeObject<ManufacturerResponseModel.ManufacturerRoot>(response.Content.ReadAsStringAsync().Result);
            await _logger.InsertLogAsync(Core.Domain.Logging.LogLevel.Information, "Call to Get BrightPearl Manufactorer END");
            return model;
        }
        #endregion
        #region Customer/Contact
        [HttpGet]
        public async Task GetCustomer(int firstResult = 0, List<int> customerIds = null)
        {
            try
            {
                customerIds = customerIds ?? new List<int>();
                var queryString = firstResult > 0 ? "?firstResult=" + firstResult : "";
                var response = await _apiHttpRequestService.HttpGetRequest(null, "contact-service/contact" + queryString);
                var customerResponse = JToken.Parse(response.Content.ReadAsStringAsync().Result).ToObject<CustomerResponseModel.Root>();
                    //var totalProducts = customerResponse.response..resultsAvailable;
                    foreach (var resItem in customerResponse.response)
                    {

                        var checkCustomerexits = await _customerMWService.GetCustomerByIdAsync(resItem.contactId);
                        if (checkCustomerexits.Count() == 0)
                        {
                            var output = CommonHelper.EnsureNotNull(resItem.communication.emails.PRI.email);
                            output = output.Trim();
                            output = CommonHelper.EnsureMaximumLength(output, 255);

                            if (CommonHelper.IsValidEmail(output))
                            {
                                await InsertNewsLetterBrighttoNop(resItem.communication.emails.PRI.email, resItem.marketingDetails.isReceiveEmailNewsletter);
                            }
                            var mwcId = resItem.contactId;
                            Customer customer = await _customerApiService.GetCustomerByMWCId(mwcId) ?? new Customer();
                            customer.MWCId = mwcId;
                            customer.CreatedOnUtc = resItem.createdOn;
                            customer.LastActivityDateUtc = resItem.lastContactedOn;
                            customer.FirstName = resItem.firstName;
                            customer.LastName = resItem.lastName;
                            customer.Salutation = resItem.salutation;
                            customer.BillingAddressId = await InsertCustomerAddressFromBrighttoNop(resItem.postAddressIds.BIL);
                            customer.ShippingAddressId = await InsertCustomerAddressFromBrighttoNop(resItem.postAddressIds.DEL);
                            customer.DefaultId = await InsertCustomerAddressFromBrighttoNop(resItem.postAddressIds.DEF);
                            customer.Email = resItem.communication.emails.PRI.email;
                            if (resItem.communication.emails.SEC != null)
                            {
                                customer.SecondaryEmail = resItem.communication.emails.SEC.email;
                            }
                            if (resItem.communication.emails.TER != null)
                            {
                                customer.TertiaryEmail = resItem.communication.emails.TER.email;
                            }
                            if (resItem.communication.telephones != null)
                            {
                                customer.Phone = resItem.communication.telephones.PRI;
                            }
                            if (resItem.communication.telephones != null)
                            {
                                customer.SecondaryTelephone = resItem.communication.telephones.SEC ?? null;
                            }
                            if (resItem.communication.telephones != null)
                            {
                                customer.MobileTelephone = resItem.communication.telephones.MOB;
                            }
                            if (resItem.communication.messagingVoips != null)
                            {
                                customer.Skype = resItem.communication.messagingVoips.SKP;
                            }
                            if (resItem.communication.websites != null)
                            {
                                customer.PrimaryWebsite = resItem.communication.websites.PRI;
                            }
                            customer.isSupplier = resItem.relationshipToAccount.isSupplier;
                            customer.isStaff = resItem.relationshipToAccount.isStaff;
                            customer.tradeStatus = resItem.tradeStatus;
                            customer.createdByid = resItem.createdByid;
                            customer.aliases = resItem.aliases.eBay;
                            customer.companyId = resItem.companyId;
                            customer.CurrentcontactStatusId = resItem.contactStatus.current.contactStatusId;
                            customer.CurrentcontactStatusName = resItem.contactStatus.current.contactStatusName;
                            if (resItem.financialDetails != null)
                            {
                                customer.priceListId = resItem.financialDetails.priceListId;
                                customer.nominalCode = resItem.financialDetails.nominalCode;
                                customer.taxCodeId = await InsertTaxcode(resItem.financialDetails.taxCodeId);
                                customer.creditLimit = resItem.financialDetails.creditLimit;
                                customer.creditTermDays = resItem.financialDetails.creditTermDays;
                                customer.discountPercentage = resItem.financialDetails.discountPercentage;
                                customer.taxNumber = resItem.financialDetails.taxNumber;
                            }
                            if (resItem.contactStatus.history != null)
                            {
                                foreach (var item in resItem.contactStatus.history)
                                {
                                    ContactHistory history = new ContactHistory();
                                    history.contactStatusName = item.contactStatusName;
                                    history.contactStatusId = item.contactStatusId;
                                    history.occuredOn = item.occuredOn;
                                    history.staffContactId = item.staffContactId;
                                    history.MWCId = resItem.contactId;
                                    await _contactHistoryServices.InsertContractHistory(history);
                                }
                            }
                            await _customerService.InsertCustomerAsync(customer);
                        }
                        else
                        {
                            var output = CommonHelper.EnsureNotNull(resItem.communication.emails.PRI.email);
                            output = output.Trim();
                            output = CommonHelper.EnsureMaximumLength(output, 255);

                            if (CommonHelper.IsValidEmail(output))
                            {
                                await InsertNewsLetterBrighttoNop(resItem.communication.emails.PRI.email, resItem.marketingDetails.isReceiveEmailNewsletter);
                            }
                            var mwcId = resItem.contactId;
                            Customer customer = await _customerApiService.GetCustomerByMWCId(mwcId) ?? new Customer();
                            customer.MWCId = mwcId;
                            customer.CreatedOnUtc = resItem.createdOn;
                            customer.LastActivityDateUtc = resItem.lastContactedOn;
                            customer.FirstName = resItem.firstName;
                            customer.LastName = resItem.lastName;
                            customer.Salutation = resItem.salutation;
                            customer.BillingAddressId = await UpdateCustomerAddressFromBrighttoNop(resItem.postAddressIds.BIL);
                            customer.ShippingAddressId = await UpdateCustomerAddressFromBrighttoNop(resItem.postAddressIds.DEL);
                            customer.DefaultId = await UpdateCustomerAddressFromBrighttoNop(resItem.postAddressIds.DEF);
                            customer.Email = resItem.communication.emails.PRI.email;
                            if (resItem.communication.emails.SEC != null)
                            {
                                customer.SecondaryEmail = resItem.communication.emails.SEC.email;
                            }
                            if (resItem.communication.emails.TER != null)
                            {
                                customer.TertiaryEmail = resItem.communication.emails.TER.email;
                            }
                            if (resItem.communication.telephones != null)
                            {
                                customer.Phone = resItem.communication.telephones.PRI;
                            }
                            if (resItem.communication.telephones != null)
                            {
                                customer.SecondaryTelephone = resItem.communication.telephones.SEC ?? null;
                            }
                            if (resItem.communication.telephones != null)
                            {
                                customer.MobileTelephone = resItem.communication.telephones.MOB;
                            }
                            if (resItem.communication.messagingVoips != null)
                            {
                                customer.Skype = resItem.communication.messagingVoips.SKP;
                            }
                            if (resItem.communication.websites != null)
                            {
                                customer.PrimaryWebsite = resItem.communication.websites.PRI;
                            }
                            customer.isSupplier = resItem.relationshipToAccount.isSupplier;
                            customer.isStaff = resItem.relationshipToAccount.isStaff;
                            customer.tradeStatus = resItem.tradeStatus;
                            customer.createdByid = resItem.createdByid;
                            customer.aliases = resItem.aliases.eBay;
                            customer.companyId = resItem.companyId;
                            customer.CurrentcontactStatusId = resItem.contactStatus.current.contactStatusId;
                            customer.CurrentcontactStatusName = resItem.contactStatus.current.contactStatusName;
                            if (resItem.financialDetails != null)
                            {
                                customer.priceListId = resItem.financialDetails.priceListId;
                                customer.nominalCode = resItem.financialDetails.nominalCode;
                                customer.taxCodeId = await InsertTaxcode(resItem.financialDetails.taxCodeId);
                                customer.creditLimit = resItem.financialDetails.creditLimit;
                                customer.creditTermDays = resItem.financialDetails.creditTermDays;
                                customer.discountPercentage = resItem.financialDetails.discountPercentage;
                                customer.taxNumber = resItem.financialDetails.taxNumber;
                            }
                            if (resItem.contactStatus.history != null)
                            {
                                foreach (var item in resItem.contactStatus.history)
                                {
                                    ContactHistory history = new ContactHistory();
                                    history.contactStatusName = item.contactStatusName;
                                    history.contactStatusId = item.contactStatusId;
                                    history.occuredOn = item.occuredOn;
                                    history.staffContactId = item.staffContactId;
                                    history.MWCId = resItem.contactId;
                                    await _contactHistoryServices.InsertContractHistory(history);
                                }
                            }
                            await _customerService.UpdateCustomerAsync(customer);
                        }
                    }
            }
            catch (Exception ex)
            {

                await _logger.InsertLogAsync(Core.Domain.Logging.LogLevel.Error, "BrightPearl to Nop product synch error : " + ex.Message);
            }
        }
        public async Task UpdateCustomer(int firstResult = 0, List<int> customerIds = null)
        {
            try
            {
                customerIds = customerIds ?? new List<int>();
                var queryString = firstResult > 0 ? "?firstResult=" + firstResult : "";
                var response = await _apiHttpRequestService.HttpGetRequest(null, "contact-service/contact" + queryString);
                var customerResponse = JToken.Parse(response.Content.ReadAsStringAsync().Result).ToObject<CustomerResponseModel.Root>();
                foreach (var resItem in customerResponse.response)
                {

                    var checkCustomerexits = await _customerMWService.GetCustomerByIdAsync(resItem.contactId);
                    if (checkCustomerexits == null)
                    {
                        throw new Exception("Customer Not Found in Nop");
                    }
                    else
                    {
                        var output = CommonHelper.EnsureNotNull(resItem.communication.emails.PRI.email);
                        output = output.Trim();
                        output = CommonHelper.EnsureMaximumLength(output, 255);

                        if (CommonHelper.IsValidEmail(output))
                        {
                            await InsertNewsLetterBrighttoNop(resItem.communication.emails.PRI.email, resItem.marketingDetails.isReceiveEmailNewsletter);
                        }
                        var mwcId = resItem.contactId;
                        Customer customer = await _customerApiService.GetCustomerByMWCId(mwcId) ?? new Customer();
                        customer.MWCId = mwcId;
                        customer.CreatedOnUtc = resItem.createdOn;
                        customer.LastActivityDateUtc = resItem.lastContactedOn;
                        customer.FirstName = resItem.firstName;
                        customer.LastName = resItem.lastName;
                        customer.Salutation = resItem.salutation;
                        customer.BillingAddressId = await UpdateCustomerAddressFromBrighttoNop(resItem.postAddressIds.BIL);
                        customer.ShippingAddressId = await UpdateCustomerAddressFromBrighttoNop(resItem.postAddressIds.DEL);
                        customer.DefaultId = await UpdateCustomerAddressFromBrighttoNop(resItem.postAddressIds.DEF);
                        customer.Email = resItem.communication.emails.PRI.email;
                        if (resItem.communication.emails.SEC != null)
                        {
                            customer.SecondaryEmail = resItem.communication.emails.SEC.email;
                        }
                        if (resItem.communication.emails.TER != null)
                        {
                            customer.TertiaryEmail = resItem.communication.emails.TER.email;
                        }
                        if (resItem.communication.telephones != null)
                        {
                            customer.Phone = resItem.communication.telephones.PRI;
                        }
                        if (resItem.communication.telephones != null)
                        {
                            customer.SecondaryTelephone = resItem.communication.telephones.SEC ?? null;
                        }
                        if (resItem.communication.telephones != null)
                        {
                            customer.MobileTelephone = resItem.communication.telephones.MOB;
                        }
                        if (resItem.communication.messagingVoips != null)
                        {
                            customer.Skype = resItem.communication.messagingVoips.SKP;
                        }
                        if (resItem.communication.websites != null)
                        {
                            customer.PrimaryWebsite = resItem.communication.websites.PRI;
                        }
                        customer.isSupplier = resItem.relationshipToAccount.isSupplier;
                        customer.isStaff = resItem.relationshipToAccount.isStaff;
                        customer.tradeStatus = resItem.tradeStatus;
                        customer.createdByid = resItem.createdByid;
                        customer.aliases = resItem.aliases.eBay;
                        customer.companyId = resItem.companyId;
                        customer.CurrentcontactStatusId = resItem.contactStatus.current.contactStatusId;
                        customer.CurrentcontactStatusName = resItem.contactStatus.current.contactStatusName;
                        if (resItem.financialDetails != null)
                        {
                            customer.priceListId = resItem.financialDetails.priceListId;
                            customer.nominalCode = resItem.financialDetails.nominalCode;
                            customer.taxCodeId = await InsertTaxcode(resItem.financialDetails.taxCodeId);
                            customer.creditLimit = resItem.financialDetails.creditLimit;
                            customer.creditTermDays = resItem.financialDetails.creditTermDays;
                            customer.discountPercentage = resItem.financialDetails.discountPercentage;
                            customer.taxNumber = resItem.financialDetails.taxNumber;
                        }
                        if (resItem.contactStatus.history != null)
                        {
                            foreach (var item in resItem.contactStatus.history)
                            {
                                ContactHistory history = new ContactHistory();
                                history.contactStatusName = item.contactStatusName;
                                history.contactStatusId = item.contactStatusId;
                                history.occuredOn = item.occuredOn;
                                history.staffContactId = item.staffContactId;
                                history.MWCId = resItem.contactId;
                                await _contactHistoryServices.InsertContractHistory(history);
                            }
                        }
                        await _customerService.UpdateCustomerAsync(customer);
                    }
                }
            }


            catch (Exception ex)
            {

                await _logger.InsertLogAsync(Core.Domain.Logging.LogLevel.Error, "BrightPearl to Nop product synch error : " + ex.Message);
            }
        }

        public async Task InsertBrightCustomertoNop(int firstResult = 0, List<int> customerIds = null)
        {
            
            try
            {
                customerIds = customerIds ?? new List<int>();
                var queryString = firstResult > 0 ? "?firstResult=" + firstResult : "";
                var response = await _apiHttpRequestService.HttpGetRequest(null, "contact-service/contact" + queryString);
                var customerResponse = JToken.Parse(response.Content.ReadAsStringAsync().Result).ToObject<CustomerResponseModel.Root>();
                var lastresult = customerResponse.pagination.lastResult;
                bool morepage = customerResponse.pagination.morePagesAvailable;
                while (morepage == true)
                {
                    await _logger.InsertLogAsync(Core.Domain.Logging.LogLevel.Information, "Call Begin");
                    await GetCustomer(firstResult);
                    await _logger.InsertLogAsync(Core.Domain.Logging.LogLevel.Information, "Call End");
                    firstResult = lastresult + 1;
                    await InsertBrightCustomertoNop(firstResult);
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task<int> InsertCustomerAddressFromBrighttoNop(int id)
        {
            try
            {
                var addreess = await GetPostalAddressById(id);
                Nop.Core.Domain.Common.Address address = new Nop.Core.Domain.Common.Address();
                address.CountryId = Convert.ToInt32(addreess.CountryId);
                address.Address1 = addreess.AddressLine1;
                address.Address2 = addreess.AddressLine2;
                address.Address3 = addreess.AddressLine3;
                address.City = addreess.AddressLine4;
                address.ZipPostalCode = addreess.PostalCode;
                address.countryIsoCode = addreess.CountryIsoCode;
                address.MWAId = id;
              await _addressService.InsertAddressAsync(address);
                return address.Id;
            }
            catch (Exception)
            {

                throw;
            }
            
        }
        public async Task<int> UpdateCustomerAddressFromBrighttoNop(int id)
        {
            try
            {
                var addreess = await GetPostalAddressById(id);
                Nop.Core.Domain.Common.Address address = new Nop.Core.Domain.Common.Address();
                address.CountryId = Convert.ToInt32(addreess.CountryId);
                address.Address1 = addreess.AddressLine1;
                address.Address2 = addreess.AddressLine2;
                address.Address3 = addreess.AddressLine3;
                address.City = addreess.AddressLine4;
                address.ZipPostalCode = addreess.PostalCode;
                address.countryIsoCode = addreess.CountryIsoCode;
                address.MWAId = id;
                await _addressService.UpdateAddressAsync(address);
                return address.Id;
            }
            catch (Exception)
            {

                throw;
            }

        }

        public async Task<PostaladdressModel> GetPostalAddressById(int Id)
        {
            var response = await _apiHttpRequestService.HttpGetRequest(null, "/contact-service/postal-address/" + Id);
            var httpResonseString = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
            var model = JsonConvert.DeserializeObject<PostaladdressModel>(httpResonseString);
            return model;
        }
        public async Task<int> InsertTaxcode(int id)
        {
            if (id == 0)
            {
                return 0;
            }
            var taxCodes = await GetTaxcode(id);
            if (taxCodes.Count() == 0)
            {
                var response = await _apiHttpRequestService.HttpGetRequest(null, "/accounting-service/tax-code/" + id);
                var httpResonseString = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                var model = JsonConvert.DeserializeObject<TaxCodeResponse.Root>(httpResonseString);
                var responses = model;
                Core.Domain.Customers.TaxCode taxCode = new Core.Domain.Customers.TaxCode();
                taxCode.Code = responses.response.FirstOrDefault().code;
                taxCode.Description = responses.response.FirstOrDefault().description;
                taxCode.Rate = responses.response.FirstOrDefault().rate;
                taxCode.MWTId = responses.response.FirstOrDefault().id;
                await _taxCodeServices.InsertTaxCode(taxCode);
                return id;
            }
            else
            {
                return id;
            }
        }
        public async Task<IList<Core.Domain.Customers.TaxCode>> GetTaxcode(int id)
        {
            var taxCodes = await _taxCodeServices.GetTaxcode(id);
            return taxCodes;
        }
        public async Task<string> InsertNominalcode(int id)
        {
            if (id == 0)
            {
                return "0";
            }
            var nominalcodes = await GetNominalcodecode(id);
            if (nominalcodes.Count == 0)
            {
                var response = await _apiHttpRequestService.HttpGetRequest(null, "/accounting-service/nominal-code/" + id);
                var httpResonseString = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                var model = JsonConvert.DeserializeObject<NominalCodeResponseModel.Root>(httpResonseString);
                var respons =  model;
                NominalCodes nominalCodes = new NominalCodes();
                nominalCodes.code = respons.response.FirstOrDefault().code;
                nominalCodes.name = respons.response.FirstOrDefault().name;
                nominalCodes.type = respons.response.FirstOrDefault().type.id.ToString();
                nominalCodes.bank = respons.response.FirstOrDefault().bank;
                nominalCodes.expense = respons.response.FirstOrDefault().expense;
                nominalCodes.discount = respons.response.FirstOrDefault().discount;
                nominalCodes.editable = respons.response.FirstOrDefault().editable;
                nominalCodes.active = respons.response.FirstOrDefault().active;
                nominalCodes.taxCode = respons.response.FirstOrDefault().taxCode;
                nominalCodes.chartMapCode = respons.response.FirstOrDefault().chartMapCode;
                nominalCodes.reconcile = respons.response.FirstOrDefault().reconcile;
                nominalCodes.MWNId = respons.response.FirstOrDefault().id;
                await _nominalCodeServices.InsertNominalCode(nominalCodes);
                return id.ToString();
            }
            else
            {
                return id.ToString();
            }
        }
        public async Task<IList<Core.Domain.Customers.NominalCodes>> GetNominalcodecode(int id)
        {
            var nominalcodesCodes = await _nominalCodeServices.GetNominalCodesAsync(id);
            return nominalcodesCodes;
        }

        public async Task<List<CustomerResponseModel.Address>> GetAddressJson(JObject jObj)
        {
            List<CustomerResponseModel.Address> addresses = new List<CustomerResponseModel.Address>();
            await _logger.InsertLogAsync(Core.Domain.Logging.LogLevel.Information, "Call to check Customer Address");
            var addressFromJson = _jsonHelper.ReadJsonDynamicProperty(jObj, "response", "PostalAddresses");
            var dicaddress = _jsonHelper.CreateObjectFromNestedJson(addressFromJson);
            var recordCount = 0;
            {
                foreach (var address in dicaddress)
                {
                    var addressList = JsonConvert.SerializeObject(address);
                    CustomerResponseModel.Address address1 = JsonConvert.DeserializeObject<CustomerResponseModel.Address>(addressList);

                }
            }
            return null;
        }

        public async Task InsertNewsLetterBrighttoNop(string customerEmail, bool isNewsletterActive)
        {
            await _newsLetterSubscriptionService.InsertNewsLetterSubscriptionAsync(new NewsLetterSubscription
            {
                NewsLetterSubscriptionGuid = Guid.NewGuid(),
                Email = customerEmail,
                Active = isNewsletterActive,
                StoreId = _storeContext.GetCurrentStore().Id,
                CreatedOnUtc = DateTime.UtcNow
            });
        }

        public virtual async Task GetBrightPearlPriceListAndSaveNopCustomerRoleNop()
        {
            var response = await _apiHttpRequestService.HttpGetRequest(null, "product-service/price-list/");
            var model = JToken.Parse(response.Content.ReadAsStringAsync().Result).ToObject<CustomerRolesResponseModel.CustomerRolesRoot>();
            foreach (var item in model.response)
            {
                var customerRole = await _customerApiService.GetCustomerRoleByMWIdAsync(item.id) ?? new CustomerRole();
                customerRole.Name = item.name.text;
                customerRole.SystemName = item.name.text;
                customerRole.MWId = item.id;
                customerRole.Active = true;
                customerRole.IsSystemRole = false;
                if (customerRole.Id > 0)
                    await _customerService.UpdateCustomerRoleAsync(customerRole);
                await _customerService.InsertCustomerRoleAsync(customerRole);
            }
        }

        #endregion

        #region check Categories 

        public virtual async Task InsertBrightPearlCaegoriesToNop()
        {
            var brightCategories = await GetBrightPearlCategories();
            var nopCategories = await _categoryServices.GetAllCategoriesAsync();
            foreach (var brighPearlCategory in brightCategories.response.OrderBy(x => x.id))
            {

                var nopCategory = nopCategories.Where(c => c.MWCId == brighPearlCategory.id).FirstOrDefault() ?? new Category();
                nopCategory.Name = brighPearlCategory.name;
                nopCategory.CreatedOnUtc = DateTime.UtcNow;
                nopCategory.Published = brighPearlCategory.active;
                nopCategory.ParentCategoryId = brighPearlCategory.parentId > 0 ? nopCategories.FirstOrDefault(x => x.MWCId == brighPearlCategory.parentId).Id : brighPearlCategory.parentId;
                nopCategory.MWCId = brighPearlCategory.id;
                nopCategory.CreatedOnUtc = DateTime.UtcNow;
                nopCategory.PageSize = _catalogSettings.DefaultManufacturerPageSize;
                nopCategory.PageSizeOptions = _catalogSettings.DefaultManufacturerPageSizeOptions;
                nopCategory.AllowCustomersToSelectPageSize = true;
                nopCategory.Description = brighPearlCategory.description.text;
                nopCategory.AllowCustomersToSelectPageSize = true;
                nopCategory.PriceRangeFiltering = false;
                nopCategory.PriceFrom = decimal.Zero;
                nopCategory.PriceTo = decimal.Zero;
                nopCategory.ManuallyPriceRange = false;
                nopCategory.DisplayOrder = 1;
                if (nopCategory != null && nopCategory.Id > 0)
                {
                    nopCategory.UpdatedOnUtc = DateTime.UtcNow;
                    await _categoryServices.UpdateCategoryAsync(nopCategory);
                }
                else
                {
                    await _categoryServices.InsertCategoryAsync(nopCategory);
                    nopCategories.Add(nopCategory);
                }

                var SeNamecat = await _urlRecordService.ValidateSeNameAsync(nopCategory, "", nopCategory.Name, true);
                await _urlRecordService.SaveSlugAsync(nopCategory, SeNamecat, 0);
            }

        }
        protected virtual async Task<CategoryResponse.CategorySearchResponseModel.CategoryRoot> GetBrightPearlCategories()
        {
            var response = await _apiHttpRequestService.HttpGetRequest(null, "product-service/brightpearl-category");
            var httpResonseString = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
            var model = JsonConvert.DeserializeObject<CategoryResponse.CategorySearchResponseModel.CategoryRoot>(httpResonseString);
            return model;
        }
        public async Task<CategoryResponse.CategoryResponseModel.CategoryResponseRoot> GetBrightPearlCategory(string categoryname)
        {
            // bool CheckCategory = false;
            var response = await _apiHttpRequestService.HttpGetRequest(null, "product-service/brightpearl-category-search?name=" + categoryname);
            var httpResonseString = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
            var model = JsonConvert.DeserializeObject<CategoryResponse.CategoryResponseModel.CategoryResponseRoot>(httpResonseString);
            //foreach (var item in model)
            //{
            //    if (item.response.FirstOrDefault().name.ToUpper() == categoryname.ToUpper())
            //    {
            //        CheckCategory = true;
            //    }
            //}
            return model;
        }

        public async Task<CategoryResponse.CategoryResultModel> GetBrightPearlCategory(string categoryname, int categoryCode)
        {
            string queryParam = string.Empty;
            if (categoryCode > 0)
                queryParam = "?id=" + categoryCode;
            else if (!string.IsNullOrEmpty(categoryname))
                queryParam = "?name=" + categoryname;
            else if (!string.IsNullOrEmpty(categoryname) && categoryCode > 0)
                queryParam = string.Format("?id={0}&name={1}", categoryname, categoryCode);

            // bool CheckCategory = false;
            var response = await _apiHttpRequestService.HttpGetRequest(null, "product-service/brightpearl-category-search" + queryParam);
            var httpResonseString = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
            var model = JsonConvert.DeserializeObject<CategoryResponse.CategoryResponseModel.CategoryResponseRoot>(httpResonseString);
            CategoryResponse.CategoryResultModel category = new CategoryResponse.CategoryResultModel();
            if (model.response.results.Any())
            {
                var firstCategory = model.response.results.FirstOrDefault();
                category.id = Convert.ToInt32(firstCategory[0]);
                category.name = firstCategory[1].ToString();
                category.parentId = Convert.ToInt32(firstCategory[2]);
            }
            return category;
        }
        public async Task<int> InsertNopCategoryToBrightPearl(string categoryName, int parentId = 0)
        {

            CategoryRequestModel model = new CategoryRequestModel
            {
                parentId = parentId,
                name = categoryName
            };
            var jsonString = JsonConvert.SerializeObject(model);
            var response = await _apiHttpRequestService.HttpPostRequest(jsonString, "product-service/brightpearl-category");
            return JToken.Parse(response.Content.ReadAsStringAsync().Result).ToObject<int>();


        }
        protected virtual async Task<ProductRequestRoot> PrepareProductRequestModel(Product product)
        {
            await _logger.InsertLogAsync(Core.Domain.Logging.LogLevel.Information, "Call to prepare product request model");
            ProductRequestModel productRequestModel = new ProductRequestModel();
            ProductRequestRoot productRequestRoot = new ProductRequestRoot();

            //check existing brightpearl brand
            var nopProductAssociatedManufacturer = (await _manufacturerServices.GetProductManufacturersByProductIdAsync(product.Id)).FirstOrDefault();
            if (product.MWPId > 0)
            {
                productRequestRoot.brandId = (await _manufacturerServices.GetManufacturerByIdAsync(nopProductAssociatedManufacturer.ManufacturerId)).MWMId;
            }
            else
            {

                //foreach (var nopProductManufacturer in nopProductAssociatedManufacturer) // not looping because brightpearl only associated with single brand
                //{
                if (nopProductAssociatedManufacturer != null)
                {
                    var manufacturer = await _manufacturerServices.GetManufacturerByIdAsync(nopProductAssociatedManufacturer.ManufacturerId);
                    var existingBrightPearlBrands = await GetExistingBrightPearlBrand(manufacturer.Name, 0);
                    if (existingBrightPearlBrands.response.results != null && existingBrightPearlBrands.response.results.Count > 0)
                        productRequestRoot.brandId = Convert.ToInt32(existingBrightPearlBrands.response.results.FirstOrDefault()[0]);
                    else
                        productRequestRoot.brandId = await InsertManufacturerInBrightPearl(manufacturer.Name, manufacturer.Description);
                }
                else
                    productRequestRoot.brandId = Convert.ToInt32((await GetExistingBrightPearlBrand("Other", 0)).response.results.FirstOrDefault()[0]); //We are assuming that Bright pearl will always have Other brand


                //}
            }

            //check existing brightpearl Category
            var salesChannelCategories = await PrepareProductRequestCategoryModel(productRequestRoot, product);

            // productRequestRoot.brandId = _manufacturerServices.GetProductManufacturersByProductIdAsync(product.Id).Id;
            productRequestRoot.collectionId = null; //need to assign later after discuss
            productRequestRoot.productTypeId = null;  //Need to assign value after discussing in team

            productRequestRoot.identity.sku = product.Sku;
            productRequestRoot.identity.ean = product.ean;
            productRequestRoot.identity.isbn = product.isbn;
            productRequestRoot.identity.barcode = product.barcode;
            productRequestRoot.identity.upc = product.upc;
            productRequestRoot.stock.weight.magnitude = Convert.ToDouble(product.Weight);
            productRequestRoot.stock.dimensions.length = product.Height.ToString();
            productRequestRoot.stock.dimensions.width = product.Width.ToString();

            // Sales Channel
            //var saleChannel = productRequestRoot.salesChannels.FirstOrDefault();
            ProductRequestModel.SalesChannel saleChannel = new ProductRequestModel.SalesChannel();
            saleChannel.salesChannelName = "Brightpearl"; //Temporary for now
            saleChannel.productName = product.Name;
            saleChannel.description.languageCode = "en"; //For now En only need to change dynamic
            saleChannel.description.text = string.IsNullOrEmpty(product.FullDescription) ? "NA" : product.FullDescription;
            saleChannel.description.format = "PLAINTEXT";
            saleChannel.shortDescription.languageCode = "en";
            saleChannel.shortDescription.text = string.IsNullOrEmpty(product.ShortDescription) ? "NA" : product.ShortDescription;
            saleChannel.shortDescription.format = "PLAINTEXT";
            saleChannel.categories = salesChannelCategories;
            productRequestRoot.salesChannels.Add(saleChannel);
            productRequestRoot.nominalCodePurchases = product.NominalCodePurchases;
            productRequestRoot.nominalCodeSales = product.NominalCodeSales;
            productRequestRoot.nominalCodeStock = product.NominalCodeStock;

            //variations
            await PrepareBrightPearlVariations(productRequestRoot, product.Id);
            return productRequestRoot;

        }

        protected virtual async Task<List<ProductRequestModel.Category>> PrepareProductRequestCategoryModel(ProductRequestRoot productRequestRoot, Product product)
        {
            await _logger.InsertLogAsync(Core.Domain.Logging.LogLevel.Information, "Call to prepare product category request model");
            var productAssociatedCategories = await _categoryServices.GetProductCategoriesByProductIdAsync(product.Id);
            List<ProductRequestModel.Category> categories = new List<ProductRequestModel.Category>();
            if (productAssociatedCategories.Count > 0)
            {
                var nopCategories = await _categoryServices.GetAllCategoriesAsync();
                ProductRequestModel.SalesChannel salesChannel = new ProductRequestModel.SalesChannel();
                // List<ProductRequestModel.Category> categories = new List<ProductRequestModel.Category>();
                foreach (var productAssociatedCategory in productAssociatedCategories)
                {
                    ProductRequestModel.Category brightPearlCategoryRequestModel = new ProductRequestModel.Category();
                    var nopCategory = nopCategories.Where(category => category.Id == productAssociatedCategory.CategoryId).FirstOrDefault();
                    var brightPearlCategory = await GetBrightPearlCategory(nopCategory.Name);
                    if (brightPearlCategory.response != null && brightPearlCategory.response.results.Any())
                    {
                        brightPearlCategoryRequestModel.categoryCode = brightPearlCategory.response.results.FirstOrDefault()[0].ToString();
                    }
                    else
                    {
                        if (nopCategory.ParentCategoryId > 0)
                        {
                            var parentCategory = nopCategories.Where(x => x.Id == nopCategory.ParentCategoryId).FirstOrDefault();
                            brightPearlCategoryRequestModel.categoryCode = (await CheckBrightPearlCategoryExist(parentCategory)).ToString();
                        }
                        else
                        {
                            brightPearlCategoryRequestModel.categoryCode = (await InsertNopCategoryToBrightPearl(nopCategory.Name, 0)).ToString();
                        }
                    }

                    //salesChannel.categories.Add(brightPearlCategoryRequestModel);
                    categories.Add(brightPearlCategoryRequestModel);
                }

            }
            else
            {
                var defaultBrightCategory = await GetBrightPearlCategory("Other", 0);
                ProductRequestModel.Category brightPearlCategoryRequestModel = new ProductRequestModel.Category();
                brightPearlCategoryRequestModel.categoryCode = defaultBrightCategory.id.ToString();
                categories.Add(brightPearlCategoryRequestModel);
            }
            return categories;
        }

        public virtual async Task<int> CheckBrightPearlCategoryExist(Category category)
        {
            List<CategoryRequestModel> categoryRequestModel = new List<CategoryRequestModel>();
            var nopCategories = await _categoryServices.GetAllCategoriesAsync();
            int categoryId = 0;
            categoryId = (await GetBrightPearlCategory("Other", 0)).id; // because category is require in brightpearl thats why passing other
            var brightPearlCategory = await GetBrightPearlCategory(category.Name, 0);
            if (brightPearlCategory != null && brightPearlCategory.id > 0)
            {
                return brightPearlCategory.parentId;
                //categoryRequestModel.Add(new CategoryRequestModel { name = brightPearlCategory.name, parentId = brightPearlCategory.parentId });
            }
            if (brightPearlCategory.name == null)
            {
                if (category.ParentCategoryId > 0)
                {
                    var parentCategory = nopCategories.Where(x => x.Id == category.ParentCategoryId).FirstOrDefault();
                    return await CheckBrightPearlCategoryExist(parentCategory);
                }
                else
                {
                    return await InsertNopCategoryToBrightPearl(category.Name, categoryId);
                }
                //var parentCategory = nopCategories.Where(x => x.Id == category.ParentCategoryId).FirstOrDefault();
                //categoryId = await CheckBrightPearlCategoryExist(parentCategory);
            }
            return categoryId;
        }

        [HttpGet]
        public async Task<CategoryResponse.CategorySearchResponseModel.CategoryRoot> GetBrightPearlCategoryByCode(string Code)
        {
            await _logger.InsertLogAsync(Core.Domain.Logging.LogLevel.Information, "Call to Get BrightPearl Category ");
            var response = await _apiHttpRequestService.HttpGetRequest(null, "product-service/brightpearl-category/" + Code);
            //string url = "https://use1.brightpearlconnect.com/public-api/midwestworkwear/product-service/brightpearl-category/" + Code;
            //var response = await _apiHttpRequestService.HttpGetRequesttest(null, url);

            //get products
            var model = JToken.Parse(response.Content.ReadAsStringAsync().Result).ToObject<CategoryResponse.CategorySearchResponseModel.CategoryRoot>();
            await _logger.InsertLogAsync(Core.Domain.Logging.LogLevel.Information, "Call to Get BrightPearl Category END ");
            //var model = JsonConvert.DeserializeObject<List<CategoryResponse>>(response.Content.ReadAsStringAsync().Result);
            return model;
        }
        #endregion

        #region BrightPearlToNop Product

        public async Task InsertBrightPearlProductsToNop(int firstResult = 0, List<int> productIds = null)
        {
            // Insert Categories to Nop First
            await InsertBrightPearlCaegoriesToNop();
            await InsertBrightPearlBrandToNop();
            await InsertCustomFieldMetadataBrightToNop();
            productIds = productIds ?? new List<int>();
            try
            {
                var queryString = firstResult > 0 ? "?firstResult=" + firstResult : "";
                var response = await _apiHttpRequestService.HttpGetRequest(null, "product-service/product-search/" + queryString);
                var productList = JToken.Parse(response.Content.ReadAsStringAsync().Result).ToObject<ProductSearchRoot>();
                var totalProducts = productList.response.metaData.resultsAvailable;
                var lastResult = productList.response.metaData.lastResult;

                productIds.AddRange(productList.response.results.Select(x => Convert.ToInt32(x[0])).ToList());

                firstResult = lastResult + 1;
                while (lastResult < productList.response.metaData.resultsAvailable)
                {
                    await _logger.InsertLogAsync(Core.Domain.Logging.LogLevel.Information, "Call Begin");
                    await InsertBrightPearlProductToNop(productIdSet: productIds);
                    await _logger.InsertLogAsync(Core.Domain.Logging.LogLevel.Information, "Call End");

                    await InsertBrightPearlProductsToNop(firstResult: firstResult);
                }
            }
            catch (Exception ex)
            {
                await _logger.InsertLogAsync(Core.Domain.Logging.LogLevel.Error, "BrightPearl to Nop product synch error : " + ex.Message);
            }
        }

        public async Task InsertBrightPearlProductToNop(int mwpId)
        {
            //var storeScope = await _storeContext.GetActiveStoreScopeConfigurationAsync();
            var response = await _apiHttpRequestService.HttpGetRequest(null, "product-service/product/" + mwpId);
            await InsertBrightPearlProductToNop(response);
        }
        public async Task InsertBrightPearlProductToNop(int prdId = 0, List<int> productIdSet = null)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            int skipRecord = 1;
            int takeRecord = 199;
            int totalRecords = productIdSet != null ? productIdSet.Count() : 0;
            List<string> productIdSetStringList = new List<string>();
            if (productIdSet != null)
            {
                while (totalRecords > 0)
                {
                    var endRecord = Convert.ToInt32(productIdSet.Take(skipRecord).LastOrDefault()) + 199;
                    productIdSetStringList.Add(string.Format("{0}-{1}", productIdSet.Take(skipRecord).LastOrDefault().ToString(), endRecord));
                    totalRecords = totalRecords - 200;
                    skipRecord = skipRecord + 200;
                }
            }

            if (productIdSetStringList.Count > 0)
            {
                foreach (var productSetString in productIdSetStringList)
                {
                    response = await _apiHttpRequestService.HttpGetRequest(null, "product-service/product/" + productSetString);
                    await _logger.InsertLogAsync(Core.Domain.Logging.LogLevel.Information, "Call Begin with productset : " + productSetString);
                    await InsertBrightPearlProductToNop(response);
                    await InsertProductCustomFields(productSetString);
                    await _logger.InsertLogAsync(Core.Domain.Logging.LogLevel.Information, "Call end with productset : " + productSetString);
                }
            }
        }

        public async Task<string> InsertBrightPearlProductToNopWithRange(int prodIdFrom = 0, int prodIdTo = 0)
        {
            try
            {
                await InsertBrightPearlCaegoriesToNop();
                await InsertBrightPearlBrandToNop();
                await InsertCustomFieldMetadataBrightToNop();
                if (prodIdFrom > 0 && prodIdTo > 0)
                {
                    if (prodIdTo <= prodIdFrom)
                    {
                        await _customLogger.LogWrite("Error", await _localizationService.GetResourceAsync("Admin.Plugin.Api.BrightPearlToNopRange.Error"));
                        return await _localizationService.GetResourceAsync("Admin.Plugin.Api.BrightPearlToNopRange.Error");
                    }

                    int totalRecords = prodIdTo - prodIdFrom;
                    List<string> productIdSetStringList = new List<string>();
                    while (totalRecords > 0)
                    {
                        prodIdTo = totalRecords < 200 ? prodIdFrom + totalRecords : prodIdFrom + 199;
                        productIdSetStringList.Add(string.Format("{0}-{1}", prodIdFrom, prodIdTo));
                        totalRecords = totalRecords - 200;
                        prodIdFrom = prodIdTo + 1;
                    }

                    if (productIdSetStringList.Count > 0)
                    {
                        foreach (var productSetString in productIdSetStringList)
                        {
                            var response = await _apiHttpRequestService.HttpGetRequest(null, "product-service/product/" + productSetString);
                            await InsertBrightPearlProductToNop(response);
                            await InsertProductCustomFields(productSetString);
                            //await InsertBrightPearlProductToNop(prdId, response);
                        }
                    }
                }

                return "";
            }
            catch (Exception ex)
            {
                await _customLogger.LogWrite("Error", ex.Message);
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #region NopToBrightPearl brand
        public async Task<int> InsertManufacturerInBrightPearl(string manufactureName, string description)
        {
            BrandRequestModel requestModel = new BrandRequestModel
            {
                name = manufactureName,
                description = description
            };

            var jsonString = JsonConvert.SerializeObject(requestModel);
            var response = await _apiHttpRequestService.HttpPostRequest(jsonString, "product-service/brand/");
            return JToken.Parse(response.Content.ReadAsStringAsync().Result).ToObject<int>();
        }
        public virtual async Task<BrandResponseModel.BrandResponseRoot> GetExistingBrightPearlBrand(string brandName, int brandId)
        {
            string queryParam = string.Empty;
            if (brandId > 0)
                queryParam = "?brandId=" + brandId;
            else if (!string.IsNullOrEmpty(brandName))
                queryParam = "?brandName=" + brandName;
            else if (!string.IsNullOrEmpty(brandName) && brandId > 0)
                queryParam = string.Format("?brandName={0}&brandId={1}", brandName, brandId);

            var response = await _apiHttpRequestService.HttpGetRequest(null, "product-service/brand-search" + queryParam);
            return JToken.Parse(response.Content.ReadAsStringAsync().Result).ToObject<BrandResponseModel.BrandResponseRoot>();

        }
        public async Task<int> ChecktheExistingBrightPearlBrand(Manufacturer manufacturer)
        {
            var response = await GetExistingBrightPearlBrand(manufacturer.Name, manufacturer.Id);
            if (response.response == null)
            {
                return await InsertManufacturerInBrightPearl(manufacturer.Name, manufacturer.Description);
            }
            else
            {
                return manufacturer.MWMId;
            }
        }
        #endregion

        #region BrightPearlToNop Brand

        public virtual async Task InsertBrightPearlBrandToNop()
        {
            var brightPearlBrands = await GetBrightPearlBrands();
            var nopManufacturers = await _manufacturerServices.GetAllManufacturersAsync();
            foreach (var brightPearlBrand in brightPearlBrands.response)
            {
                var nopManufacturer = nopManufacturers.FirstOrDefault(x => x.MWMId == brightPearlBrand.id) ?? new Manufacturer();
                nopManufacturer.Name = brightPearlBrand.name;
                nopManufacturer.Published = true;
                nopManufacturer.Description = brightPearlBrand.description;
                nopManufacturer.MWMId = brightPearlBrand.id;
                nopManufacturer.CreatedOnUtc = DateTime.UtcNow;

                if (nopManufacturer != null && nopManufacturer.Id > 0)
                {
                    nopManufacturer.UpdatedOnUtc = DateTime.UtcNow;
                    await _manufacturerServices.UpdateManufacturerAsync(nopManufacturer);
                }
                else
                {
                    await _manufacturerServices.InsertManufacturerAsync(nopManufacturer);
                    nopManufacturers.Add(nopManufacturer);
                }

                var SeNamecat = await _urlRecordService.ValidateSeNameAsync(nopManufacturer, "", nopManufacturer.Name, true);
                await _urlRecordService.SaveSlugAsync(nopManufacturer, SeNamecat, 0);
            }
        }

        protected virtual async Task<ManufacturerResponseModel.ManufacturerRoot> GetBrightPearlBrands()
        {
            var response = await _apiHttpRequestService.HttpGetRequest(null, "product-service/brand");
            var httpResonseString = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
            var model = JsonConvert.DeserializeObject<ManufacturerResponseModel.ManufacturerRoot>(httpResonseString);
            return model;
        }

        #endregion
        #region Options/OptionsValues
        public virtual async Task<OptionsResponseModel.OptionResponseRoot> GetOptionsFromBrightPearl()
        {
            var response = await _apiHttpRequestService.HttpGetRequest(null, "product-service/option-search/");
            return JToken.Parse(response.Content.ReadAsStringAsync().Result).ToObject<OptionsResponseModel.OptionResponseRoot>();
        }

        public virtual async Task<OptionsResponseModel.OptionResponseRoot> GetOptionValuesFromBrightPearl()
        {
            var response = await _apiHttpRequestService.HttpGetRequest(null, "product-service/option-value-search/");
            return JToken.Parse(response.Content.ReadAsStringAsync().Result).ToObject<OptionsResponseModel.OptionResponseRoot>();
        }

        public virtual async Task<CommonPostResponse> InsertOptionInBrightPearl(string optionName, int sortOrder = 0)
        {
            OptionRequestModel requestModel = new OptionRequestModel
            {
                name = optionName,
                sortOrder = sortOrder
            };

            var jsonString = JsonConvert.SerializeObject(requestModel);
            var response = await _apiHttpRequestService.HttpPostRequest(jsonString, "product-service/option/");
            return JToken.Parse(response.Content.ReadAsStringAsync().Result).ToObject<CommonPostResponse>();

        }

        public virtual async Task<CommonPostResponse> InsertOptionValueInBrightPearl(string optionValueName, int optionId, int sortOrder = 0)
        {
            OptionValueRequestModel requestModel = new OptionValueRequestModel
            {
                optionValueName = optionValueName,
                sortOrder = sortOrder
            };

            var jsonString = JsonConvert.SerializeObject(requestModel);
            var response = await _apiHttpRequestService.HttpPostRequest(jsonString, "product-service/option/" + optionId + "/value");
            return JToken.Parse(response.Content.ReadAsStringAsync().Result).ToObject<CommonPostResponse>();

        }



        #endregion
        #region NopToBrightPearl Products
        public async Task InsertNopProductsToBrightPearlProductsAsync()
        {
            try
            {
                var products = (await _productService.SearchProductsAsync()).Where(x => !x.IsNopToBrightPearlSynched);
                foreach (var product in products)
                {
                    await InsertNopProductToBrightPearlAsync(product);
                }
            }
            catch (Exception ex)
            {
                await _logger.InsertLogAsync(Core.Domain.Logging.LogLevel.Error, ex.Message);
            }
        }

        public async Task InsertNopProductToBrightPearlAsync(Product product)
        {
            if (product != null)
            {
                await _logger.InsertLogAsync(Core.Domain.Logging.LogLevel.Information, "Call to synch Nop Product with Id = " + product.Id);
                var productRequestModel = await PrepareProductRequestModel(product);
                var jsonString = JsonConvert.SerializeObject(productRequestModel);
                var response = await _apiHttpRequestService.HttpPostRequest(jsonString, "product-service/product");
                var brightPearlProductPostResponse = JToken.Parse(response.Content.ReadAsStringAsync().Result).ToObject<CommonPostResponse>();
                if (brightPearlProductPostResponse.response > 0)
                {
                    product.MWPId = brightPearlProductPostResponse.response;
                    product.IsNopToBrightPearlSynched = true;
                }
                await _productService.UpdateProductAsync(product);
                await InsertNopTierPricetoBrightpearl(product.MWPId);
            }
        }

        public async Task DeleteBrightPearlProductAsync(int mwpId)
        {
            var response = await _apiHttpRequestService.HttpGetRequest(null, "product-service/product/" + mwpId);
            var brightPearlProduct = JToken.Parse(response.Content.ReadAsStringAsync().Result).ToObject<ProductResponseModel.ProductRoot>();
            if (brightPearlProduct != null)
            {
                brightPearlProduct.response.FirstOrDefault().status = "ARCHIVED";
                var jsonString = JsonConvert.SerializeObject(brightPearlProduct.response.FirstOrDefault());
                await _apiHttpRequestService.HttpPutRequest(jsonString, "product-service/product/" + mwpId);
            }
        }
        #endregion
        #region Tier Prices
        public async Task InsertNopTierPricetoBrightpearl(int MWPId)
        {
            var listofProducts = await _productServiceMW.GetProductByIdAsync(MWPId);
            foreach (var product in listofProducts)
            {
                var tierPrices = await _productService.GetTierPricesByProductAsync(product.Id);
                foreach (var item in tierPrices)
                {
                    List<PriceList> tier = new List<PriceList>();
                    var tierPrice = new PriceList();
                    var customerrole = await _customerService.GetCustomerRoleByIdAsync(Convert.ToInt32(item.CustomerRoleId));
                    int brightPerlsPriceListId = 0;
                    if (customerrole == null)
                    {
                        brightPerlsPriceListId = await GetPriceListfromBrightPearl("COST");
                    }
                    else
                    {
                        brightPerlsPriceListId = await GetPriceListfromBrightPearl(customerrole.Name);
                    }
                    tierPrice.priceListId = brightPerlsPriceListId;
                    tierPrice.sku = product.Sku;
                    tierPrice.quantityPrice = new QuantityPrice()
                    {
                        Quantity = item.Quantity.ToString(),
                        Price = item.Price.ToString()
                    };
                    tier.Add(tierPrice);
                    var body = JsonConvert.SerializeObject(tier, new JsonSerializerSettings
                    {
                        NullValueHandling = NullValueHandling.Ignore
                    });
                    var content = new StringContent(body, Encoding.UTF8, ContentType);
                    var response = await _apiHttpRequestService.HttpPutRequest(body, "/product-service/product-price/'" + MWPId + "'/price-list");
                    response.EnsureSuccessStatusCode();
                }
            }
        }
        #endregion

        #region check Categories And Manufacture

        //public async Task<bool> GetBrightPearlCategory(string categoryname)
        //{
        //    bool CheckCategory = false;
        //    var response = await _apiHttpRequestService.HttpGetRequest(null, "product-service/brightpearl-category-search?name=" + categoryname);
        //    var httpResonseString = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
        //    var model = JsonConvert.DeserializeObject<IEnumerable<CategoryResponseModel.CategoryRoot>>(httpResonseString);
        //    foreach (var item in model)
        //    {
        //        if (item.response.FirstOrDefault().name.ToUpper() == categoryname.ToUpper())
        //        {
        //            CheckCategory = true;
        //        }
        //    }
        //    return CheckCategory;
        //}

        [HttpGet]
        public async Task<bool> GetBrightPearlManufacturer(string ManufacturerName)
        {
            bool CheckManufacture = false;
            var response = await _apiHttpRequestService.HttpGetRequest(null, "product-service/brand/");
            var httpResonseString = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
            var model = JsonConvert.DeserializeObject<IEnumerable<ManufacturerResponseModel.ManufacturerRoot>>(httpResonseString);
            foreach (var item in model)
            {
                if (item.response.FirstOrDefault().name.ToUpper() == ManufacturerName.ToUpper())
                {
                    CheckManufacture = true;
                }
            }
            return CheckManufacture;
        }
        public async Task InsertNopManufacturerToBrightpearl()
        {
            var listOfManufacturer = await _manufacturerServicesMW.GetManufacturerAsync();
            foreach (var manufacturer in listOfManufacturer)
            {
                bool CheckManufacture = await GetBrightPearlManufacturer(manufacturer.Name);
                if (CheckManufacture == false)
                {
                    ManufacturerRequestModel manufacturerRequestModel = new ManufacturerRequestModel();
                    manufacturerRequestModel.name = manufacturer.Name;
                    manufacturerRequestModel.description = manufacturer.Description;
                    var body = JsonConvert.SerializeObject(manufacturerRequestModel, new JsonSerializerSettings
                    {
                        NullValueHandling = NullValueHandling.Ignore
                    });
                    var content = new StringContent(body, Encoding.UTF8, ContentType);
                    var response = await _apiHttpRequestService.HttpPostRequest(body, "/product-service/brand");
                    response.EnsureSuccessStatusCode();
                    var httpresponsestring = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                    var responseId = JsonConvert.DeserializeObject<int>(httpresponsestring);
                    manufacturer.MWMId = responseId;
                    await _manufacturerServices.UpdateManufacturerAsync(manufacturer);
                }
            }
        }

        public async Task<int> GetPriceListfromBrightPearl(string customerroleName)
        {
            int priceListId = 0;
            var store = _storeContext.GetCurrentStore();
            var response = await _apiHttpRequestService.HttpGetRequest(null, "product-service/price-list");
            var jObj = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            var responseString = _jsonHelper.ReadJsonDynamicProperty(jObj, "response", "priceLists");
            var jObjPriceList = JObject.Parse(JsonConvert.SerializeObject(responseString));
            var model = JToken.Parse(response.Content.ReadAsStringAsync().Result).ToObject<IEnumerable<PriceListResponseModel.PriceList>>();
            foreach (var item in model)
            {
                if (item.response.FirstOrDefault().code == customerroleName)
                {
                    priceListId = item.response.FirstOrDefault().id;
                }
            }
            return priceListId;
        }
        #endregion

        #region Utilities

        protected virtual async Task InsertBrightPearlProductToNop(HttpResponseMessage response, int prdId = 0)
        {

            var jObj = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            var jObject = JToken.Parse(response.Content.ReadAsStringAsync().Result).ToObject<ProductObjectResponse>();

            var products = JToken.Parse(response.Content.ReadAsStringAsync().Result).ToObject<ProductResponseModel.ProductRoot>();

            #region Warehouse from response
            var warehouseProductMappingList = await WarehouseList(jObj);
            #endregion
            foreach (var item in products.response)
            {
                await _logger.InsertLogAsync(Core.Domain.Logging.LogLevel.Information, "Synch Prod Id = " + item.id);
                var product = await _productApiService.GetProductByMWPId(item.id) ?? new Product();
                if (item.salesChannels.FirstOrDefault() != null)
                {
                    var productWarehouse = warehouseProductMappingList.Where(x => x.productId == item.id);
                    //product
                    await Task.Run(() => PrepareBrightToNopProductModel(item, product));
                    product.WarehouseId = productWarehouse != null && productWarehouse.Count() > 0 ? productWarehouse.FirstOrDefault().warehouseMWId : 0;

                    if (product.Id > 0)
                        await _productService.UpdateProductAsync(product);
                    else
                        await _productService.InsertProductAsync(product);

                    await InsertPrice(product);
                    ////product attributes
                    ///
                    if (item.variations != null)
                        await SaveProductAttributes(item.variations, product.Id);

                    //search engine name
                    var SeName = await _urlRecordService.ValidateSeNameAsync(product, "", product.Name, true);
                    await _urlRecordService.SaveSlugAsync(product, SeName, 0);

                    await PrepareBrightToNopCategoryModel(item, product);

                    if (item.brandId != 0)
                    {
                        await PrepareBrightToNopBrandModel(item, product);
                    }
                }
            }
        }
        public virtual async Task SaveProductAttributes(List<ProductResponseModel.Variation> variations, int productId)
        {
            var allAttributes = await _productAttributeService.GetAllProductAttributesAsync();
            var allProductAttributeMapping = await _productAttributeService.GetProductAttributeMappingsByProductIdAsync(productId);

            foreach (var variation in variations)
            {
                var productAttribute = new ProductAttribute();
                productAttribute.MWPAId = variation.optionId;
                productAttribute.Name = variation.optionName;
                var isExistProductAttribute = allAttributes != null ? allAttributes.Contains(productAttribute) : false;
                if (!isExistProductAttribute)
                    await _productAttributeService.InsertProductAttributeAsync(productAttribute);
                else
                    await _productAttributeService.UpdateProductAttributeAsync(productAttribute);
                if (productAttribute.Id > 0)
                {
                    var productAttributeMapping = new ProductAttributeMapping();
                    productAttributeMapping.ProductId = productId;
                    productAttributeMapping.ProductAttributeId = productAttribute.Id;
                    productAttributeMapping.IsRequired = true;
                    productAttributeMapping.AttributeControlType = AttributeControlType.RadioList; //Temporary for now

                    var isExistProductAttributeMapping = allProductAttributeMapping.Contains(productAttributeMapping);
                    if (!isExistProductAttributeMapping)
                        await _productAttributeService.InsertProductAttributeMappingAsync(productAttributeMapping);
                    else
                        await _productAttributeService.UpdateProductAttributeMappingAsync(productAttributeMapping);

                    if (productAttributeMapping.Id > 0)
                    {
                        var productAttributeValue = new ProductAttributeValue();
                        productAttributeValue.ProductAttributeMappingId = productAttributeMapping.Id;
                        productAttributeValue.Name = variation.optionValue;
                        productAttributeValue.MWPAVId = variation.optionValueId;

                        var allProductAttributeValues = await _productAttributeService.GetProductAttributeValuesAsync(productAttributeMapping.Id);

                        if (allProductAttributeValues != null && allProductAttributeValues.Count() > 0)
                            await _productAttributeService.UpdateProductAttributeValueAsync(productAttributeValue);
                        else
                            await _productAttributeService.InsertProductAttributeValueAsync(productAttributeValue);

                    }
                }

            }
        }

        public virtual async Task<List<WarehouseProductMappingResponseModel>> WarehouseList(JObject jObj)
        {
            List<WarehouseProductMappingResponseModel> warehouseProductMappingResponseModelList = new List<WarehouseProductMappingResponseModel>();
            await _logger.InsertLogAsync(Core.Domain.Logging.LogLevel.Information, "Call to check warehouse WarehouseList ");
            var warehousesFromJson = _jsonHelper.ReadJsonDynamicProperty(jObj, "response", "warehouses", "id");
            var dicWareHouses = _jsonHelper.CreateObjectFromNestedJson(warehousesFromJson);
            var recordCount = 0;
            foreach (var warehouseRes in dicWareHouses.Values)
            {
                var warehouseListJson = JsonConvert.SerializeObject(warehouseRes);
                ProductWarehouseResponseModel warehouseResModel = JsonConvert.DeserializeObject<ProductWarehouseResponseModel>(warehouseListJson);
                var warehouseEntity = await _warehouseApiService.GetWarehouseByMWWId(Convert.ToInt32(warehouseResModel.Id));
                WarehouseProductMappingResponseModel warehouseProductMappingModel = new WarehouseProductMappingResponseModel();
                warehouseProductMappingModel.productId = Convert.ToInt32(dicWareHouses.ToArray()[recordCount].Key);
                warehouseProductMappingModel.warehouseMWId = warehouseEntity.Id;
                warehouseProductMappingResponseModelList.Add(warehouseProductMappingModel);
                recordCount++;
            }
            return warehouseProductMappingResponseModelList;
        }
        public virtual async Task<List<WarehouseProductMappingResponseModel>> SaveWarehouse(JObject jObj)
        {

            var warehousesFromJson = _jsonHelper.ReadJsonDynamicProperty(jObj, "response", "warehouses", "id");
            var dicWareHouses = _jsonHelper.CreateObjectFromNestedJson(warehousesFromJson);
            var recordCount = 0;
            List<WarehouseProductMappingResponseModel> warehouseProductMappingResponseModelList = new List<WarehouseProductMappingResponseModel>();
            // var warehouseListJson = JsonConvert.SerializeObject(dicWareHouses);
            // List<ProductWarehouseResponseModel> warehouseResListModel = JsonConvert.DeserializeObject<List<ProductWarehouseResponseModel>>(dicWareHouses.Values);
            //ProductWarehouseResponseModel warehouseResModel = JsonConvert.DeserializeObject<ProductWarehouseResponseModel>(warehouseListJson);
            await _logger.InsertLogAsync(Core.Domain.Logging.LogLevel.Information, "Call to check warehouse ");
            foreach (var warehouseRes in dicWareHouses.Values)
            {

                var warehouseListJson = JsonConvert.SerializeObject(warehouseRes);
                ProductWarehouseResponseModel warehouseResModel = JsonConvert.DeserializeObject<ProductWarehouseResponseModel>(warehouseListJson);
                var response = await _apiHttpRequestService.HttpGetRequest(null, "warehouse-service/warehouse/" + warehouseResModel.Id);
                var warehouseEntity = await _warehouseApiService.GetWarehouseByMWWId(Convert.ToInt32(warehouseResModel.Id)) ?? new Warehouse();
                var warehouseResponse = JToken.Parse(response.Content.ReadAsStringAsync().Result).ToObject<WarehouseRoot>();

                foreach (var item in warehouseResponse.response)
                {
                    WarehouseProductMappingResponseModel warehouseProductMappingModel = new WarehouseProductMappingResponseModel();
                    Warehouse warehouse = new Warehouse();
                    warehouse.MWWId = warehouseResponse.response.FirstOrDefault().id;
                    warehouse.Name = warehouseResponse.response.FirstOrDefault().name;
                    warehouse.AddressId = await SaveAddress(warehouseResponse.response.FirstOrDefault().address);

                    if (warehouseEntity.Id > 0)
                    {
                        warehouse.Id = warehouseEntity.Id;
                        await _shippingService.UpdateWarehouseAsync(warehouse);
                    }
                    else
                        await _shippingService.InsertWarehouseAsync(warehouse);

                    warehouseProductMappingModel.productId = Convert.ToInt32(dicWareHouses.ToArray()[recordCount].Key);
                    warehouseProductMappingModel.warehouseMWId = warehouse.Id;
                    warehouseProductMappingResponseModelList.Add(warehouseProductMappingModel);
                }
                recordCount++;
            }
            await _logger.InsertLogAsync(Core.Domain.Logging.LogLevel.Information, "Call to check warehouse END");
            return warehouseProductMappingResponseModelList;
        }

        public virtual async Task<int> SaveAddress(WarehouseResponseModel.Address addressModel)
        {
            if (addressModel.addressId > 0)
            {
                var address = _addressApiService.GetAddressByMWAId(addressModel.addressId) ?? new Core.Domain.Common.Address();
                //NEed to implement Customer also as per ContactId in response It can be done with Customer synching
                address.MWAId = addressModel.addressId;
                address.Address1 = addressModel.streetAddress;
                address.City = addressModel.city;
                address.ZipPostalCode = addressModel.postcode;
                await SaveCountry(addressModel.countryId, addressModel.countryIsoCode2);
                address.CountryId = addressModel.countryId;
                address.StateProvinceId = await SaveStates(addressModel.state, addressModel.countryId);
                if (address.Id > 0)
                    await _addressService.UpdateAddressAsync(address);
                else
                    await _addressService.InsertAddressAsync(address);
                return address.Id;
            }
            return 0;

        }


        public virtual async Task SaveCountry(int countryId, string countryCode)
        {
            var country = await _countryService.GetCountryByTwoLetterIsoCodeAsync(countryCode) ?? new Core.Domain.Directory.Country();
            if (country != null)
            {
                country.MWCId = countryId;
                await _countryService.UpdateCountryAsync(country);
            }
            else
            {
                country.MWCId = countryId;
                country.TwoLetterIsoCode = countryCode;
                country.Published = true;
                country.Name = countryCode; //As we dont have countryname
                await _countryService.InsertCountryAsync(country);
            }
        }

        public virtual async Task<int> SaveStates(string stateAbbrevation, int countryId)
        {
            var state = await _stateProvinceService.GetStateProvinceByAbbreviationAsync(stateAbbrevation) ?? new Core.Domain.Directory.StateProvince();
            if (state == null)
            {
                state.Name = stateAbbrevation; //we dont have state name
                state.Abbreviation = stateAbbrevation;
                state.CountryId = countryId;
                state.Published = true;
                await _stateProvinceService.InsertStateProvinceAsync(state);

            }
            return state.Id;
        }

        public virtual async Task InsertPrice(Product product)
        {
            var store = _storeContext.GetCurrentStore();
            var response = await _apiHttpRequestService.HttpGetRequest(null, "product-service/product-price/" + product.MWPId);
            var jObj = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            // var jsonString = "{\'response\':[{\'productId\':1000,\'priceLists\':[{\'priceListId\':1,\'currencyCode\':\'USD\',\'currencyId\':1,\'quantityPrice\':{\'1\':\'0\', \'5\':\'5\'}},{\'priceListId\':2,\'currencyCode\':\'USD\',\'currencyId\':1,\'quantityPrice\':{}},{\'priceListId\':3,\'currencyCode\':\'USD\',\'currencyId\':1,\'quantityPrice\':{}},{\'priceListId\':4,\'currencyCode\':\'USD\',\'currencyId\':1,\'quantityPrice\':{}},{\'priceListId\':5,\'currencyCode\':\'USD\',\'currencyId\':1,\'quantityPrice\':{}},{\'priceListId\':8,\'currencyCode\':\'USD\',\'currencyId\':1,\'quantityPrice\':{}},{\'priceListId\':9,\'currencyCode\':\'USD\',\'currencyId\':1,\'quantityPrice\':{}},{\'priceListId\':10,\'currencyCode\':\'USD\',\'currencyId\':1,\'quantityPrice\':{}},{\'priceListId\':11,\'currencyCode\':\'USD\',\'currencyId\':1,\'quantityPrice\':{}},{\'priceListId\':12,\'currencyCode\':\'USD\',\'currencyId\':1,\'quantityPrice\':{}},{\'priceListId\':13,\'currencyCode\':\'USD\',\'currencyId\':1,\'quantityPrice\':{}}]}]}";
            //var jObj = JObject.Parse(jsonString);
            var responseString = _jsonHelper.ReadJsonDynamicProperty(jObj, "response", "priceLists");
            var jObjPriceList = JObject.Parse(JsonConvert.SerializeObject(responseString));
            var model = JToken.Parse(response.Content.ReadAsStringAsync().Result).ToObject<ProductPriceResponseModel.ProductPriceRoot>();
            var customerRoles = await _customerService.GetAllCustomerRolesAsync();
            foreach (var item in model.response.FirstOrDefault().priceLists)
            {
                await _logger.InsertLogAsync(Core.Domain.Logging.LogLevel.Information, "Call to Insert Price ");
                var tierPrices = _jsonHelper.ReadJsonDynamicProperty(jObjPriceList, "priceLists", "quantityPrice", "priceListId");
                var tierPriceWithCustomerRoleObject = _jsonHelper.CreateTierPriceListByCustomerRole(tierPrices);
                foreach (var dic in tierPriceWithCustomerRoleObject)
                {
                    var customerRole = customerRoles.Where(x => x.MWId == Convert.ToInt32(dic.Key)).FirstOrDefault();
                    var existingTierPrices = await _productService.GetTierPricesByProductAsync(product.Id);
                    foreach (var dicValue in dic.Value)
                    {
                        var existingTierPriceDetail = existingTierPrices.FirstOrDefault(x => x.CustomerRoleId == customerRole.Id && x.ProductId == product.Id && x.StoreId == store.Id && x.Quantity == Convert.ToInt32(dicValue.Key));
                        TierPrice tierPrice = new TierPrice();
                        tierPrice.CustomerRoleId = customerRole.Id;
                        tierPrice.ProductId = product.Id;
                        tierPrice.Quantity = Convert.ToInt32(dicValue.Key);
                        tierPrice.Price = Convert.ToDecimal(dicValue.Value);
                        tierPrice.StoreId = _storeContext.GetCurrentStore().Id;
                        if (existingTierPriceDetail != null)
                        {
                            tierPrice.Id = existingTierPriceDetail.Id;
                            await _productService.UpdateTierPriceAsync(tierPrice);
                        }
                        else
                            await _productService.InsertTierPriceAsync(tierPrice);
                    }
                }
            }
            await _logger.InsertLogAsync(Core.Domain.Logging.LogLevel.Information, "Call to Insert Price END");
        }

        protected virtual void PrepareBrightToNopProductModel(ProductResponseModel.Response responseModel, Product product)
        {
            var ProductChannel = responseModel.salesChannels.FirstOrDefault();
            var Identity = responseModel.identity;
            //product.WarehouseId = productWarehouse != null && productWarehouse.Count() > 0 ? productWarehouse.FirstOrDefault().warehouseMWId : 0;
            //product
            product.Name = ProductChannel.productName;
            product.VisibleIndividually = true;
            product.Published = true;
            product.ProductTypeId = (int)ProductType.SimpleProduct;
            product.StockQuantity = 10000;
            product.OrderMaximumQuantity = 10000;
            product.CreatedOnUtc = DateTime.UtcNow;
            product.UpdatedOnUtc = DateTime.UtcNow;
            product.Sku = Identity.sku;
            product.MWPId = responseModel.id;
            product.featured = responseModel.featured;
            product.productGroupId = responseModel.id;
            product.barcode = Identity.barcode;
            product.mpn = Identity.mpn;
            product.upc = Identity.upc;
            product.IsShipEnabled = true;
            product.ProductCondition = ProductChannel.productCondition;
            product.NominalCodeStock = responseModel.nominalCodeStock;
            product.NominalCodeSales = responseModel.nominalCodeSales;
            product.NominalCodePurchases = responseModel.nominalCodePurchases;
            product.PrimarySupplierId = responseModel.primarySupplierId;
            product.SalesPopupMessage = responseModel.salesPopupMessage;
            product.WarehousePopupMessage = responseModel.warehousePopupMessage;
            product.Published = responseModel.status == "LIVE" ? true : false;
            product.Deleted = responseModel.status == "ARCHIVED" ? true : false;
            product.IsBrightPearlToNopSynched = true;
            //product.Version = item.version;

            if (ProductChannel.description != null)
                product.FullDescription = ProductChannel.description.text;

            if (ProductChannel.shortDescription != null)
                product.ShortDescription = ProductChannel.shortDescription.text;

            if (responseModel.stock != null)
            {
                if (responseModel.stock.stockTracked)
                {
                    product.ManageInventoryMethod = ManageInventoryMethod.ManageStock;
                }

                product.Weight = responseModel.stock.weight.magnitude;
                if (responseModel.stock.dimensions != null)
                {
                    product.Length = responseModel.stock.dimensions.length;
                    product.Width = responseModel.stock.dimensions.width;
                    product.Height = responseModel.stock.dimensions.height;
                    product.Volume = responseModel.stock.dimensions.volume;
                }
            }

        }

        protected virtual async Task PrepareBrightToNopCategoryModel(ProductResponseModel.Response responseModel, Product product)
        {
            var ProductChannel = responseModel.salesChannels.FirstOrDefault();
            var Categories = ProductChannel.categories.OrderBy(x => x.categoryCode);
            var nopCategories = (await _categoryServices.GetAllCategoriesAsync()).Where(x => x.MWCId > 0);
            foreach (var cat in Categories)
            {
                var nopCategory = nopCategories.FirstOrDefault(x => x.MWCId == Convert.ToInt32(cat.categoryCode));
                var existingProductCategoryMapping = await _categoryServices.GetProductCategoriesByProductIdAsync(product.Id);
                var isExistProductCategoryMapping = existingProductCategoryMapping != null && existingProductCategoryMapping.Any() ? existingProductCategoryMapping.Select(x => x.CategoryId == nopCategory.Id).Any() : false;
                if (!isExistProductCategoryMapping && nopCategory != null)
                {
                    await _categoryServices.InsertProductCategoryAsync(new ProductCategory
                    {
                        ProductId = product.Id,
                        CategoryId = nopCategory.Id,
                        DisplayOrder = 0
                    });
                }
                else if(nopCategory != null)
                {
                    // Need to change code later on , its temporary change
                    var productCategory = existingProductCategoryMapping.Where(x => x.CategoryId == nopCategory.Id).FirstOrDefault();
                    if (productCategory != null)
                        await _categoryServices.UpdateProductCategoryAsync(productCategory);
                }
            }
        }

        protected virtual async Task PrepareBrightToNopBrandModel(ProductResponseModel.Response responseModel, Product product)
        {

            var manufacturer = await _manufacturerServicesMW.GetExistingManufacturerAsync(responseModel.brandId) ?? new Manufacturer();

            var existingProductManufacturerMapping = await _manufacturerServices.GetProductManufacturersByProductIdAsync(product.Id);
            var isExistProductManufacturerMapping = (existingProductManufacturerMapping != null && existingProductManufacturerMapping.Any()) ? existingProductManufacturerMapping.Where(x => x.ManufacturerId == manufacturer.Id).Any() : false;
            if (!isExistProductManufacturerMapping)
            {
                await _manufacturerServices.InsertProductManufacturerAsync(new ProductManufacturer
                {
                    ProductId = product.Id,
                    ManufacturerId = manufacturer.Id,
                    DisplayOrder = 0
                });
            }
            else
            {
                var existingProductManufacturer = existingProductManufacturerMapping.FirstOrDefault(x => x.ManufacturerId == manufacturer.Id);
                if(existingProductManufacturer != null)
                await _manufacturerServices.UpdateProductManufacturerAsync(existingProductManufacturer);
            }
        }

        protected virtual async Task PrepareBrightPearlVariations(ProductRequestRoot productRequestModel, int prdId)
        {
            await _logger.InsertLogAsync(Core.Domain.Logging.LogLevel.Information, "Call to prepare product variation model");
            var productAttributesMapping = await _productAttributeService.GetProductAttributeMappingsByProductIdAsync(prdId);
            var brightPearlOptions = await GetOptionsFromBrightPearl();
            var brightPearlOptionValues = await GetOptionValuesFromBrightPearl();
            List<ProductRequestModel.Variation> variationList = new List<ProductRequestModel.Variation>();
            foreach (var productAttributeMapping in productAttributesMapping)
            {
                ProductRequestModel.Variation variation = new ProductRequestModel.Variation();
                var attribute = await _productAttributeService.GetProductAttributeByIdAsync(productAttributeMapping.ProductAttributeId);
                var attributeValues = await _productAttributeService.GetProductAttributeValuesAsync(productAttributeMapping.ProductAttributeId);
                var alreadyExistBrightPearlOption = brightPearlOptions.response.results.Where(x => x[1].ToString().Equals(attribute.Name, StringComparison.InvariantCultureIgnoreCase));
                // Check Attribute 
                if (!alreadyExistBrightPearlOption.Any())
                {
                    await _logger.InsertLogAsync(Core.Domain.Logging.LogLevel.Information, "Call to Insert variation with name =" + attribute.Name);
                    variation.optionId = (await InsertOptionInBrightPearl(attribute.Name)).response;
                }
                else
                    variation.optionId = Convert.ToInt32(alreadyExistBrightPearlOption.FirstOrDefault()[0]);

                //check attrbiute value
                foreach (var attrbiuteValue in attributeValues)
                {
                    var alreadyExixtBrightPearlOptionValues = brightPearlOptionValues.response.results.Where(x => x[1].ToString().Equals(attrbiuteValue.Name, StringComparison.InvariantCultureIgnoreCase));
                    if (!alreadyExixtBrightPearlOptionValues.Any())
                    {
                        variation.optionValueId = (await InsertOptionValueInBrightPearl(attrbiuteValue.Name, variation.optionId)).response;
                    }
                    else
                        variation.optionValueId = Convert.ToInt32(alreadyExixtBrightPearlOptionValues.FirstOrDefault()[0]);
                    variationList.Add(variation);
                }
            }
        }

        protected virtual SpecificationAttributeType CompareAttributeControlTypeWithBrightPearl(string brightControlType)
        {
            switch (brightControlType)
            {
                case "YES_NO":
                case "SELECT":
                    return SpecificationAttributeType.Option;
                case "TEXT":
                    return SpecificationAttributeType.CustomText;
                
                default:
                    return SpecificationAttributeType.CustomText;
            }
        }
        #endregion

        #region BrightPearlToNop Warehousr
        public async Task InsertBrightPearlWarehouseToNop()
        {
            var response = await _apiHttpRequestService.HttpGetRequest(null, "warehouse-service/warehouse");
            var warehouseList = JToken.Parse(response.Content.ReadAsStringAsync().Result).ToObject<WarehouseRoot>();
            foreach (var warehouse in warehouseList.response)
            {
                var warehouseEntity = await _warehouseApiService.GetWarehouseByMWWId(Convert.ToInt32(warehouse.id));
                if (warehouseEntity != null)
                {
                    warehouseEntity.MWWId = warehouse.id;
                    warehouseEntity.Name = warehouse.name;
                    warehouseEntity.AddressId = await SaveAddress(warehouse.address);
                    await _shippingService.UpdateWarehouseAsync(warehouseEntity);
                }
                else
                {
                    warehouseEntity = new Warehouse();
                    warehouseEntity.MWWId = warehouse.id;
                    warehouseEntity.Name = warehouse.name;
                    warehouseEntity.AddressId = await SaveAddress(warehouse.address);
                    await _shippingService.InsertWarehouseAsync(warehouseEntity);
                }
            }
        }

        #endregion
        #region Webhook POST/GET
        public async Task CreateWebhook(WebhookRequestModel requestModel)
        {
            if (requestModel == null)
            {
                requestModel = new WebhookRequestModel();
                requestModel.subscribeTo = "product";
                requestModel.httpMethod = "POST";
                requestModel.idSetAccepted = true;
                requestModel.qualityOfService = 0;
                requestModel.contentType = "application/json";
                requestModel.uriTemplate = "https://midwestworkwear.sigmasolve.com/callback";

                WebhookBodyTemplate bodyTemplate = new WebhookBodyTemplate();
                //bodyTemplate.accountCode = "midwestworkwearsandbox";
                //bodyTemplate.brightpearlVersion = "4.23.1";
                //bodyTemplate.resourceType = "product";
                //bodyTemplate.id = "8";
                //bodyTemplate.lifecycleEvent = "created";
                //bodyTemplate.fullEvent = "product.created";

                requestModel.bodyTemplate = JsonConvert.SerializeObject(bodyTemplate);

                var response = await _apiHttpRequestService.HttpPostRequest(JsonConvert.SerializeObject(requestModel), "/integration-service/webhook");
                var finalResponse = "a";

            }
        }

        #endregion

        #region Bright Pearl Custom Fields

        public virtual async Task InsertCustomFieldMetadataBrightToNop()
        {
            var response = await _apiHttpRequestService.HttpGetRequest(null, "product-service/product/custom-field-meta-data/");
            var customFieldOptions = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            var customFieldOptionObject = _jsonHelper.ReadJsonDynamicProperty(customFieldOptions, "response", "options", "id");
            var customFieldOptionList = _jsonHelper.CreateListFromNestedJson(customFieldOptionObject);
            var customFieldMetadataList = JToken.Parse(response.Content.ReadAsStringAsync().Result).ToObject<CustomFieldMetadataResponseModel.Root>();
            var allSpecificationAttributes =  _specificationAttributeApiService.GetSpecificationAttributes(); 
            foreach (var customFieldMetadata in customFieldMetadataList.response)
            {
                var customFieldOptionsList = customFieldOptionList != null ? customFieldOptionList.FirstOrDefault(x => x.Key == customFieldMetadata.id.ToString()).Value : null;
                var specificationAttribute = new SpecificationAttribute();
                specificationAttribute.Name = customFieldMetadata.name;
                specificationAttribute.MWSId = customFieldMetadata.id;
                specificationAttribute.MWCustomFieldCode = customFieldMetadata.code;
                specificationAttribute.MWCustomFieldType = customFieldMetadata.customFieldType;
                var existNopSpecificationAttribute = (allSpecificationAttributes != null && allSpecificationAttributes.Count > 0) ? allSpecificationAttributes.FirstOrDefault(x => x.Name.Equals(customFieldMetadata.name, StringComparison.InvariantCultureIgnoreCase)) ?? new SpecificationAttribute() : null;
                if (existNopSpecificationAttribute != null && existNopSpecificationAttribute.Id > 0)
                {
                    specificationAttribute.Id = existNopSpecificationAttribute.Id;
                    await _specificationService.UpdateSpecificationAttributeAsync(specificationAttribute);
                }
                else
                    await _specificationService.InsertSpecificationAttributeAsync(specificationAttribute);

                if (customFieldOptionsList != null && customFieldMetadata.options != null)
                {
                    foreach (var customFieldOption in customFieldOptionsList)
                    {
                        var customFieldOptionValueObject = JsonConvert.DeserializeObject<CustomFieldMetadataOptionsResponseModel>(customFieldOption);
                        SpecificationAttributeOption specificationAttributeOption = new SpecificationAttributeOption();
                        specificationAttributeOption.MWOId = customFieldOptionValueObject.id;
                        specificationAttributeOption.Name = customFieldOptionValueObject.value;
                        specificationAttributeOption.SpecificationAttributeId = specificationAttribute.Id;
                        var isExistSpecificationAttributeOption = _specificationAttributeApiService.IsSpecificationAttributeOptionExist(customFieldOptionValueObject.value, specificationAttribute.Id);

                        if (isExistSpecificationAttributeOption)
                            await _specificationService.UpdateSpecificationAttributeOptionAsync(specificationAttributeOption);
                        else
                            await _specificationService.InsertSpecificationAttributeOptionAsync(specificationAttributeOption);
                    }

                }
                else if(customFieldMetadata.customFieldType == "YES_NO")
                {
                   
                    foreach(var option in staticYes_NoOptions)
                    {
                        SpecificationAttributeOption specificationAttributeOption = new SpecificationAttributeOption();
                        specificationAttributeOption.MWOId = 0;
                        specificationAttributeOption.Name = option;
                        specificationAttributeOption.SpecificationAttributeId = specificationAttribute.Id;
                        var isExistSpecificationAttributeOption = _specificationAttributeApiService.IsSpecificationAttributeOptionExist(option, specificationAttribute.Id);

                        if (isExistSpecificationAttributeOption)
                            await _specificationService.UpdateSpecificationAttributeOptionAsync(specificationAttributeOption);
                        else
                            await _specificationService.InsertSpecificationAttributeOptionAsync(specificationAttributeOption);
                    }
                    
                }
                // To insert Default Attribute option
                else
                {
                    SpecificationAttributeOption specificationAttributeOption = new SpecificationAttributeOption();
                    specificationAttributeOption.MWOId = 0;
                    specificationAttributeOption.Name = "Default";
                    specificationAttributeOption.SpecificationAttributeId = specificationAttribute.Id;
                    var isExistSpecificationAttributeOption = _specificationAttributeApiService.IsSpecificationAttributeOptionExist("Default", specificationAttribute.Id);

                    if (isExistSpecificationAttributeOption)
                        await _specificationService.UpdateSpecificationAttributeOptionAsync(specificationAttributeOption);
                    else
                        await _specificationService.InsertSpecificationAttributeOptionAsync(specificationAttributeOption);
                }
            }
        }


        public virtual async Task InsertProductCustomFields(string productRange)
        {
            var response = await _apiHttpRequestService.HttpGetRequest(null, "product-service/product-custom-field/" + productRange);
            if (response != null)
            {
                var productCustomFieldJObject = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                var productCustomFieldDic = _jsonHelper.ReadJobjectProperty(productCustomFieldJObject, "response");
                foreach (var productCustomField in productCustomFieldDic)
                {
                    var product = await _productApiService.GetProductByMWPId(Convert.ToInt32(productCustomField.Key));
                    var productCustomFieldOptions = JsonConvert.DeserializeObject<Dictionary<string, object>>(productCustomField.Value.ToString());
                    foreach (var productCustomFieldOption in productCustomFieldOptions)
                    {
                        var existingProductSpecification = await _specificationAttributeApiService.GetSpecificationAttributeAsync(code: productCustomFieldOption.Key);
                        if(existingProductSpecification != null && product != null)
                        {
                            ProductSpecificationAttribute productSpecificationAttribute = new ProductSpecificationAttribute();
                            productSpecificationAttribute.AllowFiltering = false;
                            productSpecificationAttribute.ProductId = product.Id;
                            var controlType =  CompareAttributeControlTypeWithBrightPearl(existingProductSpecification.MWCustomFieldType);
                            if (controlType == SpecificationAttributeType.Option)
                            {
                                if (existingProductSpecification.MWCustomFieldType == "YES_NO")
                                {
                                    var specificationAttributeOptionId = (await _specificationService.GetSpecificationAttributeOptionsBySpecificationAttributeAsync(existingProductSpecification.Id)).FirstOrDefault().Id;
                                    productSpecificationAttribute.SpecificationAttributeOptionId = specificationAttributeOptionId;
                                    productSpecificationAttribute.AttributeTypeId = (int)SpecificationAttributeType.Option;
                                }
                                else
                                {
                                    var selectedCustomFieldOption = JsonConvert.DeserializeObject<CustomFieldMetadataOptionsResponseModel>(productCustomFieldOption.Value.ToString());
                                    var specificationAttributeOptionId = (await _specificationAttributeApiService.GetSpecificationAttributeOptionByMWOId(selectedCustomFieldOption.id)).Id;
                                    productSpecificationAttribute.SpecificationAttributeOptionId = specificationAttributeOptionId;
                                    productSpecificationAttribute.AttributeTypeId = (int)SpecificationAttributeType.Option;
                                }
                            }
                            else
                            {
                                var specificationAttributeOptionId = (await _specificationService.GetSpecificationAttributeOptionsBySpecificationAttributeAsync(existingProductSpecification.Id)).FirstOrDefault().Id;
                                productSpecificationAttribute.SpecificationAttributeOptionId = specificationAttributeOptionId;
                                productSpecificationAttribute.CustomValue = productCustomFieldOption.Value.ToString();
                                productSpecificationAttribute.AttributeTypeId = (int)controlType;
                            }

                            await _specificationService.InsertProductSpecificationAttributeAsync(productSpecificationAttribute);

                        }
                    }
                }
            }
        }
        //public virtual async Task InsertProductCustomFields(string productRange)
        //{
        //    var response = await _apiHttpRequestService.HttpGetRequest(null, "product-service/product-custom-field/" + productRange);
        //    if (response != null)
        //    {
        //        var productCustomFieldJObject = JObject.Parse(response.Content.ReadAsStringAsync().Result);
        //        var productCustomFieldDic = _jsonHelper.ReadJobjectProperty(productCustomFieldJObject, "response");
        //        foreach (var productCustomField in productCustomFieldDic)
        //        {
        //            var productCustomFieldOptions = JsonConvert.DeserializeObject<Dictionary<string,object>>(productCustomField.Value.ToString());
        //            //ProductAttributeMapping productAttributeMapping = new ProductAttributeMapping();
        //            //productAttributeMapping.ProductId = Convert.ToInt32(productCustomField.Key);
        //            foreach(var productCustomFieldOption in productCustomFieldOptions)
        //            {
        //                var productAttribute = await _productAttributesApiService.GetProductAttributeByCode(code:productCustomFieldOption.Key);
        //                if(productAttribute != null && productAttribute.Any())
        //                {
        //                    var existingProductAttributeMappingWithProductId = await _productAttributeService.GetProductAttributeMappingsByProductIdAsync(Convert.ToInt32(productCustomField.Key));
        //                    var existingProductAttributeMappingWithPrdAndAttributeId = existingProductAttributeMappingWithProductId != null ? existingProductAttributeMappingWithProductId.FirstOrDefault(x => x.ProductAttributeId == productAttribute.FirstOrDefault().Id) ?? new ProductAttributeMapping() : new ProductAttributeMapping();
        //                    if(existingProductAttributeMappingWithPrdAndAttributeId != null && existingProductAttributeMappingWithPrdAndAttributeId.Id > 0)
        //                    {
        //                        existingProductAttributeMappingWithPrdAndAttributeId.ProductAttributeId = productAttribute.FirstOrDefault().Id;
        //                        existingProductAttributeMappingWithPrdAndAttributeId.AttributeControlTypeId = await CompareAttributeControlTypeWithBrightPearl(productAttribute.FirstOrDefault().MWCustomFieldType);
        //                        await _productAttributeService.UpdateProductAttributeMappingAsync(existingProductAttributeMappingWithPrdAndAttributeId);
        //                    }
        //                    else
        //                    {
        //                        existingProductAttributeMappingWithPrdAndAttributeId.ProductId = Convert.ToInt32(productCustomField.Key);
        //                        existingProductAttributeMappingWithPrdAndAttributeId.ProductAttributeId = productAttribute.FirstOrDefault().Id;
        //                        existingProductAttributeMappingWithPrdAndAttributeId.AttributeControlTypeId = await CompareAttributeControlTypeWithBrightPearl(productAttribute.FirstOrDefault().MWCustomFieldType);
        //                        await _productAttributeService.InsertProductAttributeMappingAsync(existingProductAttributeMappingWithPrdAndAttributeId);
        //                    }
                                

        //                    ProductAttributeValue productAttributeValue = new ProductAttributeValue();
        //                    productAttributeValue.ProductAttributeMappingId = existingProductAttributeMappingWithPrdAndAttributeId.Id;
                            
        //                    if (productCustomFieldOption.Value.GetType().Name == "JObject")
        //                    {
        //                        var productAttributeValueString = productCustomFieldOption.Value.ToString();
        //                        var productAttributeValueObject = JsonConvert.DeserializeObject< CustomFieldMetadataOptionsResponseModel>(productAttributeValueString);
        //                        productAttributeValue.Name = productAttributeValueObject.value;
        //                    }
        //                    else
        //                    {
        //                        productAttributeValue.Name = productCustomFieldOption.Value.ToString();
        //                    }
        //                    await _productAttributeService.InsertProductAttributeValueAsync(productAttributeValue);
        //                }
        //            }
        //        }

        //        var a = 0;
        //    }
        //}

        #endregion
    }

}