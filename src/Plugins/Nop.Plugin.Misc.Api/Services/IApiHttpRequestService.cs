﻿using DocumentFormat.OpenXml.EMMA;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Api.Services
{
    public partial interface IApiHttpRequestService
    {
        Task<HttpResponseMessage> HttpPostRequest(Dictionary<string,string> parameters, string token, string action);

        Task<HttpResponseMessage> HttpGetRequest(IDictionary<string, string> parameters, string action);

        Task<HttpResponseMessage> HttpPostRequest(string parameters,  string action);

        Task<HttpResponseMessage> HttpPutRequest(string parameters, string action);
        //Task<HttpResponseMessage> HttpGetRequesttest(IDictionary<string, string> parameters, string action);
        //Task<Stream> HttpRequest(IDictionary<string,string> parameters, string action);
    }
}
