﻿using System.Collections.Generic;
using System.Linq;
using Nop.Data;
using Nop.Core.Domain.Catalog;
using Nop.Plugin.Api.DataStructures;
using Nop.Plugin.Api.Infrastructure;
using System.Threading.Tasks;
using Nop.Core.Caching;
using System.Linq.Dynamic.Core;

namespace Nop.Plugin.Api.Services
{
    public class ProductAttributesApiService : IProductAttributesApiService
    {
        private readonly IRepository<ProductAttribute> _productAttributesRepository;
        private readonly IStaticCacheManager _staticCacheManager;
        public ProductAttributesApiService(IRepository<ProductAttribute> productAttributesRepository, IStaticCacheManager staticCacheManager)
        {
            _productAttributesRepository = productAttributesRepository;
            _staticCacheManager = staticCacheManager;
        }

        public IList<ProductAttribute> GetProductAttributes(
            int limit = Constants.Configurations.DefaultLimit,
            int page = Constants.Configurations.DefaultPageValue, int sinceId = Constants.Configurations.DefaultSinceId)
        {
            var query = GetProductAttributesQuery(sinceId);

            return new ApiList<ProductAttribute>(query, page - 1, limit);
        }

        public int GetProductAttributesCount()
        {
            return GetProductAttributesQuery().Count();
        }

        Task<ProductAttribute> IProductAttributesApiService.GetByIdAsync(int id)
        {
            if (id <= 0)
            {
                return null;
            }

            return _productAttributesRepository.GetByIdAsync(id);
        }

        //public async Task<List<ProductAttribute>> GetProductAttributeByCode(string code = "" , string name ="")
        //{
        //    var cacheKey = _staticCacheManager.PrepareKeyForDefaultCache(new CacheKey("ProductAttributeByCode_" + code));
        //    var productAttribute = await _staticCacheManager.GetAsync(cacheKey, async () =>
        //    {
        //        var query = _productAttributesRepository.Table;

        //        if(!string.IsNullOrEmpty(code))
        //         query = query.Where(x => x.MWCustomFieldCode == code);
        //        if(!string.IsNullOrEmpty(name))
        //            query = query.Where(x => x.Name == name);
        //        return query.ToList();
        //    });
        //    return productAttribute ;
        //}
        private IQueryable<ProductAttribute> GetProductAttributesQuery(int sinceId = Constants.Configurations.DefaultSinceId)
        {
            var query = _productAttributesRepository.Table;

            if (sinceId > 0)
            {
                query = query.Where(productAttribute => productAttribute.Id > sinceId);
            }

            query = query.OrderBy(productAttribute => productAttribute.Id);

            return query;
        }
    }
}
