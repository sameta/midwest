﻿using Irony;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Api.Services
{
    public class CustomLogger : ICustomLogger
    {
        public virtual async Task LogWrite(string logType,string message)
        {
            var fileName = string.Format("{0}_{1}.txt", "log", DateTime.Now.Date.ToString());
            var path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "//" + fileName;
            try
            {
                if (!File.Exists(path))
                {
                    FileStream stream = File.Create(path);
                    stream.Close();
                }
                await File.WriteAllTextAsync(path, string.Format("{0}:{1}", logType, message));
                
                //using (StreamWriter w = File.AppendText(path))
                //{
                //   Log(logType,message, w);
                //}
            }
            catch (Exception ex)
            {
            }
        }

        public void Log(string logType, string logMessage, TextWriter txtWriter)
        {
            try
            {
                txtWriter.Write("\r\nLog Entry : ");
                txtWriter.WriteLine("{0}:{1} {2}", logType, DateTime.Now.ToLongTimeString(),
                    DateTime.Now.ToLongDateString());
                txtWriter.WriteLine("  :");
                txtWriter.WriteLine("  :{0}", logMessage);
                txtWriter.WriteLine("-------------------------------");
                txtWriter.Dispose();
                txtWriter.Close();
            }
            catch (Exception ex)
            {
            }
        }
    }
}
