﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Core.Domain.Common
{
    public partial class Address
    {
        public int MWAId { get; set; }
        public string Address3 { get; set; }
        public string countryIsoCode { get; set; }
    }
}
