﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Core.Domain.Customers
{
    public class ContactHistory : BaseEntity
    {
        public string contactStatusName { get; set; }
        public int contactStatusId { get; set; }
        public DateTime occuredOn { get; set; }
        public int staffContactId { get; set; }
        public int MWCId { get; set; }
    }
}
