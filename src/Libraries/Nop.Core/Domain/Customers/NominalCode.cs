﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Core.Domain.Customers
{
    public partial class NominalCodes : BaseEntity
    {
        public string code { get; set; }
        public string name { get; set; }
        public string type { get; set; }
        public bool bank { get; set; }
        public bool expense { get; set; }
        public bool discount { get; set; }
        public bool editable { get; set; }
        public bool active { get; set; }
        public int taxCode { get; set; }
        public string chartMapCode { get; set; }
        public bool reconcile { get; set; }
        public int MWNId { get; set; }
    }
}
