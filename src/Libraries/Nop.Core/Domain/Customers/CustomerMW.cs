﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Core.Domain.Customers
{
    public partial class Customer
    {
        public int MWCId { get; set; }
        public string Salutation { get; set; }
        public int? DefaultId { get; set; }
        public string SecondaryEmail { get; set; }
        public string TertiaryEmail { get; set; }
        public string SecondaryTelephone { get; set; }
        public string MobileTelephone { get; set; }
        public string FaxNumber { get; set; }
        public string Skype { get; set; }
        public string PrimaryWebsite { get; set; }
        public string contactStatusName { get; set; }
        public string contactStatusId { get; set; }
        public bool isSupplier { get; set; }
        public bool isStaff { get; set; }
        public int organisationId { get; set; }
        public string OrgName { get; set; }
        public string JobTitle { get; set; }
        public string tradeStatus { get; set; }
        public int createdByid { get; set; }
        public string aliases { get; set; }
        public int companyId { get; set; }
        public string CurrentcontactStatusName { get; set; }
        public int CurrentcontactStatusId { get; set; }
        public int priceListId { get; set; }
        public string nominalCode { get; set; }
        public int taxCodeId { get; set; }
        public string creditLimit { get; set; }
        public int creditTermDays { get; set; }
        public double discountPercentage { get; set; }
        public string taxNumber { get; set; }
    }
}
