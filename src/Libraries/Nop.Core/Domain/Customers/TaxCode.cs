﻿using Nop.Core.Domain.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Core.Domain.Customers
{
    public partial class TaxCode : BaseEntity
    {
        public string Code { get; set; }
        public string Description { get; set; }
        public int? Rate { get; set; }
        public int MWTId { get; set; }
    }
}
