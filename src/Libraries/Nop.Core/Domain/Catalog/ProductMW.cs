﻿using System;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Discounts;
using Nop.Core.Domain.Localization;
using Nop.Core.Domain.Security;
using Nop.Core.Domain.Seo;
using Nop.Core.Domain.Stores;

namespace Nop.Core.Domain.Catalog
{
    public partial class Product 
    {
        public int MWPId { get; set; }

        public string ean { get; set; }

        public string isbn { get; set; }
        public string upc { get; set; }
        public string mpn { get; set; }
        public string barcode { get; set; }
        public int productGroupId { get; set; }
        public bool featured { get; set; }
        public decimal Volume { get; set; }
        public string ProductCondition { get; set; }

        public string NominalCodeStock { get; set; }

        public string NominalCodePurchases { get; set; }

        public string NominalCodeSales { get; set; }
        public int PrimarySupplierId { get; set; }
        public string SalesPopupMessage { get; set;}

        public string WarehousePopupMessage { get; set;}

        public string Version { get; set; }

        public bool IsBrightPearlToNopSynched { get; set; }

        public bool IsNopToBrightPearlSynched { get; set; }
    }
}