﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Core.Domain.Catalog
{
    public partial class CategoryInsertEvent
    {
        public CategoryInsertEvent(Category category)
        {
            Category = category;
        }
        public Category Category
        {
            get;
        }
    }
}
