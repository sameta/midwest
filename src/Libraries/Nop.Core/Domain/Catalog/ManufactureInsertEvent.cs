﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Core.Domain.Catalog
{
    public partial class ManufactureInsertEvent
    {
        public ManufactureInsertEvent(Manufacturer manufacturer)
        {
            Manufacturer = manufacturer;
        }
        public Manufacturer Manufacturer { get; set; }
    }
}
