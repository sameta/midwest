﻿using Nop.Core.Domain.Localization;

namespace Nop.Core.Domain.Catalog
{
    /// <summary>
    /// Represents a specification attribute
    /// </summary>
    public partial class SpecificationAttribute 
    {
        public int MWSId { get; set; }
        public string MWCustomFieldCode { get; set; }
        public string MWCustomFieldType { get; set; }

    }
}
