﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Core.Domain.Tax
{
	public partial class TaxCategory
	{
        public int MWId { get; set; }
        public string Description { get; set; }
    }
}
