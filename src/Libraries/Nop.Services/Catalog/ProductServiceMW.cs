﻿using Nop.Core.Domain.Catalog;
using Nop.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Services.Catalog
{
	public partial class ProductServiceMW: IProductServiceMW
	{
		private readonly IRepository<Product> _productRepository;
        public ProductServiceMW(IRepository<Product> productRepository)
        {
            _productRepository = productRepository;
        }
        public virtual async Task<IEnumerable<Product>> GetProductByIdAsync(int MWPId)
		{
            return await _productRepository.Table.Where(x=>x.MWPId==MWPId).ToListAsync();
		}

	}
}
