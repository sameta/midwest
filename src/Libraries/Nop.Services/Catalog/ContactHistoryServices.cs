﻿using Nop.Core.Domain.Customers;
using Nop.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Services.Catalog
{
    public class ContactHistoryServices : IContactHistoryServices
    {
        private readonly IRepository<ContactHistory> _repository;
        public ContactHistoryServices(IRepository<ContactHistory> repository)
        {
            _repository = repository;
        }

        public virtual async Task InsertContractHistory(ContactHistory contactHistory)
        {
            await _repository.InsertAsync(contactHistory);
        }

    }
}
