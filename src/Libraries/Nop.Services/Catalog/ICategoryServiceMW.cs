﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Nop.Core;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Discounts;

namespace Nop.Services.Catalog
{
    /// <summary>
    /// Category service interface
    /// </summary>
    public partial interface ICategoryServiceMW
    {

        Task<Category> GetExistingCategoriesAsync(int categoryId);
        Task<IEnumerable<Category>> GetCategoriesAsync();


	}
}