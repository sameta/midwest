﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Nop.Core;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Discounts;

namespace Nop.Services.Catalog
{
    /// <summary>
    /// Manufacturer service
    /// </summary>
    public partial interface IManufacturerServiceMW
    {
        Task<Manufacturer> GetExistingManufacturerAsync(int manufacturerId);
        Task<IEnumerable<Manufacturer>> GetManufacturerAsync();


	}
}