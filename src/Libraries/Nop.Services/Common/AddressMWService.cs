﻿using Nop.Core.Caching;
using Nop.Core.Domain.Common;
using Nop.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Services.Common
{
    public partial class AddressMWService : IAddressMWService
    {
        private readonly IRepository<Address> _addressRepository;
        public AddressMWService(IRepository<Address> repository)
        {
            _addressRepository = repository;
        }
        public virtual async Task<IEnumerable<Address>> GetAddressByIdAsync(int addressId)
        {
            return await _addressRepository.Table.Where(x => x.MWAId == addressId).ToListAsync();
            
        }
    }
}
