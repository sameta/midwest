﻿using Nop.Core.Domain.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Services.Common
{
    public interface IAddressMWService
    {
        Task<IEnumerable<Address>> GetAddressByIdAsync(int addressId);
    }
}
