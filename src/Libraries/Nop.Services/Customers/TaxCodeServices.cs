﻿using Nop.Core.Domain.Customers;
using Nop.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Services.Customers
{
    public class TaxCodeServices : ITaxCodeServices
    {
        private readonly IRepository<TaxCode> _repository;
        public TaxCodeServices(IRepository<TaxCode> repository)
        {
            _repository = repository;
        }

        public async Task InsertTaxCode(TaxCode taxCode)
        {
             await _repository.InsertAsync(taxCode);
        }
        public async Task<IList<TaxCode>> GetTaxcode(int id)
        {
            return await _repository.Table.Where(x => x.MWTId == id).ToListAsync();
        }
    }
}
