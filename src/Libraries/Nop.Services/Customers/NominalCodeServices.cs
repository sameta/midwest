﻿using Nop.Core.Domain.Customers;
using Nop.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Services.Customers
{
    public class NominalCodeServices : INominalCodeServices
    {
        private readonly IRepository<Core.Domain.Customers.NominalCodes> _repository;
        public NominalCodeServices(IRepository<Core.Domain.Customers.NominalCodes> repository)
        {
            _repository = repository;
        }
        public async Task InsertNominalCode(NominalCodes nominalCodes)
        {
            await _repository.InsertAsync(nominalCodes);
        }
        public async Task<IList<NominalCodes>> GetNominalCodesAsync(int Id)
        {
            return await _repository.Table.Where(x => x.MWNId == Id).ToListAsync();
        }
        public async Task UpdateNominalCode(NominalCodes nominalCodes)
        {
            await _repository.UpdateAsync(nominalCodes);
        }
    }
}
