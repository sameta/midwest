﻿using Nop.Core.Caching;
using Nop.Core.Domain.Customers;
using Nop.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Services.Customers
{
    public class CustomerMWService : ICustomerMWService
    {
        private readonly IRepository<Customer> _customerRepository;
        public CustomerMWService(IRepository<Customer> repository)
        {
            _customerRepository = repository;
        }
        public virtual async Task<IList<Customer>> GetCustomerByIdAsync(int customerId)
        {
            return await _customerRepository.Table.Where(x =>x.MWCId==customerId).ToListAsync();
        }
    }
}
