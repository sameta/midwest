﻿using Nop.Core.Domain.Customers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Services.Customers
{
    public interface INominalCodeServices
    {
        Task InsertNominalCode(NominalCodes nominalCodes);
        Task<IList<NominalCodes>> GetNominalCodesAsync(int Id);
        Task UpdateNominalCode(NominalCodes nominalCodes);
    }
}
