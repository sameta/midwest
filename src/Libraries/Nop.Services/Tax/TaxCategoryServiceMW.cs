﻿using Nop.Core.Domain.Tax;
using Nop.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Services.Tax
{
	public partial class TaxCategoryServiceMW : ITaxCategoryServiceMW
	{
		private readonly IRepository<TaxCategory> _taxCategoryRepository;
        public TaxCategoryServiceMW(IRepository<TaxCategory> taxCategoryRepository)
        {
			_taxCategoryRepository = taxCategoryRepository;
		}
		public virtual async Task<TaxCategory> GetTaxCategoryByIdAsync(int mwId)
		{
			return await _taxCategoryRepository.GetByIdAsync(mwId, cache => default);
		}
	}
}
