﻿using Nop.Core.Domain.Tax;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Services.Tax
{
	public interface ITaxCategoryServiceMW
	{
		Task<TaxCategory> GetTaxCategoryByIdAsync(int mwId);
	}
}
