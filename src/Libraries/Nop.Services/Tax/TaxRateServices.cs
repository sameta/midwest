﻿using Nop.Core.Domain.Tax;
using Nop.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Services.Tax
{
	public partial class TaxRateServices : ITaxRateServices
	{
		private readonly IRepository<TaxRates> _taxRatesRepository;
		public TaxRateServices(IRepository<TaxRates> repository)
		{
			_taxRatesRepository = repository;
		}
		public virtual async Task InsertTaxRatesAsync(TaxRates  taxRates)
		{
			await _taxRatesRepository.InsertAsync(taxRates);
		}
	}
}
