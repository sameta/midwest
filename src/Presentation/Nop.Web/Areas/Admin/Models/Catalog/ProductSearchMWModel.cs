﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Areas.Admin.Models.Common;
using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    /// <summary>
    /// Represents a product search model
    /// </summary>
    public partial record ProductSearchModel 
    {

        #region Properties

        [NopResourceDisplayName("Admin.Catalog.Products.List.RangeFrom")]
        public int RangeFrom { get; set; }

        [NopResourceDisplayName("Admin.Catalog.Products.List.RangeTo")]
        public int RangeTo { get; set; }

    
        #endregion
    }
}